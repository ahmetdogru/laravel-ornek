<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of genel
 *
 * @author ersoft-ahmet
 */
//Mailer::UsePhpMailer('mail.dgryazilim.com', 'info@dgryazilim.com', '485926,,', true, 587, 'tls');

class genell
{

	// E-posta gönderir
	public static function SendEmail($to, $subject, $body, $headers = '', $printErrors = true, $force = false)
	{
		global $SiteEmailFrom;
		$emailFrom = "$GLOBALS[SITE_ADI] <$SiteEmailFrom>";

		if($headers == '')
			$headers = "To: $to\nFrom: $emailFrom\nContent-type: text/html; charset=iso-8859-9\n";

//		if (Debug::$IsAktif)
//		{
//			$file = 'tmp/email.txt';
//			if (file_exists($file) && date('d-m-Y', filemtime($file)) != date('d-m-Y'))
//				@unlink($file);
//			$fp = fopen($file, 'a');
//			fwrite($fp, "$to [$subject] ".date('H:i:s', microtime(true))."\r\n");
//			fclose($fp);
//		}
		if ($to != 'ahmt.dgru@gmail.com' && $to != 'dogruahmet93@gmail.com' && $to != 'info@dgryazilim.com')
		{
			if(! $force && isLocalhost())
				return 0;
			if (GVar('STATUS') != 'PRODUCTION')
				return 1;
		}
		if (isLocalhost())
			$subject = '[localhost]' . $subject;
		$sonuc = Mailer::Send($to, $subject, $body, $headers, $printErrors);
		return ($sonuc ? 1 : 0);
	}

	public static function WriteInfo($desc, $perc)
	{
		$myFile = "tmp/backup.txt";
		$fh = fopen($myFile, 'w');
		if(! $fh)
			return;
		$stringData = serialize(array($desc, $perc));
		fwrite($fh, $stringData);
		fclose($fh);
	}
}
