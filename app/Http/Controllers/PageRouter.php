<?php

namespace App\Http\Controllers;

use App\Http\Controllers\frontend\staff\BilgilerimPage;
use Illuminate\Http\Request;

class PageRouter extends Controller
{
    public function route(Request $request, $method = null)
    {
        if($request->route())
            $prefix = $request->route()->getPrefix();
        if(isset($prefix))
            $act = substr($prefix,1,strlen($prefix));
        else
            $act = $request->act;
		$act2 = $request->act2;
		if($act == "ajax" && $request->isMethod('post'))
		{
			$act  = 'guest';
			$act2 = 'ajax';
		}
        $act2 = Bool($act2 == "", "index", $act2);
        $act3 = $request->act3;
        $acts = ['act' =>  $act, 'act2' => $act2, 'act3' => $act3];

        $link = Bool($act == 'admin', "backend", "frontend.".$act).".$act2". IfTrue($act3 != "", ".$act3");
        $blade = $link;
        $classPath = str_replace('.','/',$link);
        $classNameSpace = $classPath;
		if(is_dir(resource_path("views/$classPath")))
		{
			$parts = explode(".", $link);
			$bladeName = $parts[count($parts)-1];
			$blade = $link .".". $bladeName;
			$classNameSpace = $classPath =$classPath . "/$bladeName";
		}
        $classPath = app_path()."/Http/Controllers/$classPath.php";
        $controller = null;
        if(is_file($classPath))
        {
            require_once "$classPath";
            $path = pathinfo($classNameSpace);
            $className = $path['filename'] . "Page";
            $classEndName = str_replace("/","\\","App/Http/Controllers/" .$path['dirname'] . "/$className");
            if(class_exists($classEndName))
                $controller = new $classEndName($blade,$acts);
        }
        if(! is_a($controller, "App\Http\Controllers\PageController"))
            $controller = new PageController($blade,$acts);
		if($request->isMethod('post'))
			return $controller->HandleAjaxRequest();
		else
		{
			$controller->Init();
			$controller->Index();
			$controller->Render();
			return $controller->GetBladeView();
		}
    }

    public function routeWithActs($acts, $method = "get")
    {
        $request = new Request();
		$request->setMethod($method);
        foreach ($acts as $key => $value)
            $request->{$key} = $value;
        return $this->route($request);
    }
}
