<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use ReflectionClass;
use ReflectionMethod;
use App\Http\Controllers\Other\cpaint\cpaint;

class PageController extends HomeController
{
	public static $Charset = 'utf-8';
	// Sayfa Başlığı
    public $Title = "";
    public $SubTitle = "";

	//Sayfa Detay sayfası mı ?
	public $DetayPage = false;
	public $DetayModelName = false;

	/** @var PageController */
	public static $_CurrentInstance = NULL;

	public $ConvertQuots = true;

    public $Blade = "";
    public $Acts  = [];
    protected $BladeView = null;

    public function __construct($blade = "",$acts = array())
    {
        $this->Blade = $blade;
        $this->Acts = $acts;
		$this->Page = $this;

		PageController::$_CurrentInstance = $this;
    }

	public function Index()
	{
		if($this->DetayPage)
		{
			if($this->DetayModelName == "")
				die("DetayModelName belirtiniz..");
			$modelName = "App\\".$this->DetayModelName;
			$model = null;
			if(isset($_GET['id']))
				$model = $modelName::find($_GET['id']);
			if(! $model)
				$model = new $modelName();
			$this->Data = $model;
		}
		return parent::Index();
	}

    public function GetBladeView()
    {
        if($this->BladeView)
            return $this->BladeView;
        return view($this->Blade);
    }
    public function SetBladeView(View $view)
    {
        $this->BladeView = $view;
    }

    public function Render()
    {
        $view = $this->GetBladeView();
        $vars = get_object_vars($this);
        foreach ($vars as $name => $value)
            if ($name[0] != '_' && $value != NULL) {
                $view->with($name, $value);
            }
            $view->with('girisYapan',Kisi());
        $this->SetBladeView($view);
    }

    public function HandleAjaxRequest()
    {
//		is_file(app_path() . '\Http\Controllers\Other\cpaint\cpaint2.inc.php');
        // Henüz yüklenmemişse, cpaint'i yükle
        if (! class_exists('App\Http\Controllers\Other\cpaint\cpaint'))
            require_once app_path() . '\Http\Controllers\Other\cpaint\cpaint2.inc.php';

        // Metotları register et
        $GLOBALS['cp'] = $cp = new cpaint();
        $methodNames = array();
        foreach($this->GetAllowedAjaxMethods() as $method)
        {
            $class = get_class($this);
            $methodName = $method;
            $regs = array();
            if (is_array($method))
            {
                if (count($method) == 1)
                    $methodName = $method[0];
                else
                {
                    $class = $method[0];
                    $methodName = $method[1];
                }
            }
            else if (is_string($method) && preg_match("/([a-z0-9_]*)(\.|[:]{2})([a-z0-9_]*)/i", $method, $regs))
            {
                $class = $regs[1];
                $methodName = $regs[3];
            }
            $methodNames[] = $methodName;
            $cp->register(array($class, $methodName));
        }

        // Yetki kontrolü ve metot tanımları tamamlandı
        // gerekli çağrının handle edilmesi işlemini Cpaint'e bırakıyoruz
        $cp->start("UTF-8", true);
        die($cp->return_data());
    }

    /**
     * Belli bir şablona uyan tüm public metotlar Ajax tarafından çağrılabilir
     * @return string[]
     */
    public function GetAllowedAjaxMethods()
    {
        // Şablon
        $pattern = '/^[^_].*/';

        // Interface methodları çağrılamaz
        $intMethods = get_class_methods('App\Http\Controllers\PageController');

        // Metotlar
        $reflector = new ReflectionClass($this);
        $allMethods = $reflector->getMethods(ReflectionMethod::IS_PUBLIC);
        $availableMethods = array();
        foreach($allMethods as $method)
        {
            /* @var $method ReflectionMethod */
            if ($method->isStatic() &&
                preg_match($pattern, $method->name) &&
                ! in_array($method->name, array_values($intMethods)))
                $availableMethods[] = array(get_class ($this), $method->name);
        }

        return $availableMethods;
    }

	public static function IsUTF8()
	{
		return strtolower(self::$Charset) == 'utf-8';
	}
}
