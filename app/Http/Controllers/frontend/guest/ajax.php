<?php
namespace App\Http\Controllers\frontend\guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PageController;

class AjaxPage extends PageController
{
	public static function IletisimeGec($obj)
	{
		$body  = "<b>Gönderen Ad:</b> $obj->ad";
		$body .= "<br><b>Gönderen Email:</b> $obj->email";
		$body .= "<br><b>Mesaj:</b><br> $obj->mesaj";
		return MailGonder("info@dgryazilim.com", "İletişim Formu", $body);
	}
}
