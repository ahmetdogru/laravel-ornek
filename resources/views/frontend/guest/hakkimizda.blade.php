@extends('frontend.app')
@section('breadcrumb')
    <div class="uk-container uk-container-center uk-breadcrumbs-title">
        <div class="page-title">
            <span>Hakkımızda</span>
        </div>
    </div>
    <div class="uk-breadcrumbs-wrapper">
        <div class="uk-container uk-container-center">
            <ul class="uk-breadcrumb">
                <li><a href="/">Anasayfa</a></li>
                <li class="uk-active">
                    <span>Hakkımızda</span>
                </li>
            </ul>
        </div>
    </div>

@endsection
@section('content')
<div class="uk-container uk-container-center with-breadcrumbs">
            <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
                                <div class="tm-main uk-width-medium-1-1">


                                            <main class="tm-content uk-position-relative">

    <article class="uk-article">



        <div class="kc_clfw"></div><div class="tm-about-page">
  <h4 class="uk-article-title">DGR Yazılım & Bilişim</h4>
  <div class="uk-grid">
    <div class="uk-width-medium-1-2">
	<p>
		DGR Yazılım 2018 yılında, ülkemizde yazılım sektörüne yapılan yetersiz yatırım neticesinde oluşan piyasa açığını kapatmak,müşteri memmuniyeti sağlamak amacıyla Gaziantep'te kurulmuştur.
		DGR Yazılım gerek yurt içi ve gerekse yurt dışı müşterilerine,ihtiyacı olan danışmanlık,reklam ve yazılıma dair her hizmeti eksiksiz verebilmektedir.
	</p>
    </div>
    <div class="uk-width-medium-1-2">
		<p>
			Şirketimiz çalışmaları esnasında, olaya müşterilerimizin baktığı pencereden bakmayı amaçlar.Bu sayede müşterinin isteği daha rahat anlaşılır, çözüm daha rahat ve daha sonuç odaklı olur.
		</p>
    </div>
  </div>
  <div class="tm-fullwidth-icons uk-clearfix">
    <div class="tm-fullwidth-icon">
      <span class="tm-icon-wrapper"><span class="tm-icon tm-icon-1"> </span></span>
      <span class="tm-icon-title">Başlangıç</span>
      <!--<span class="tm-icon-desc">With what mingled joy and sorrow do I take up dearest friend!</span>-->
    </div>
    <div class="tm-fullwidth-icon">
      <span class="tm-icon-wrapper"><span class="tm-icon tm-icon-2"> </span></span>
      <span class="tm-icon-title">Görüşme</span>
      <!--<span class="tm-icon-desc">It was some time before he obtained any answer, and the reply.</span>-->
    </div>
    <div class="tm-fullwidth-icon">
      <span class="tm-icon-wrapper"><span class="tm-icon tm-icon-3"> </span></span>
      <span class="tm-icon-title">Mail ile Örnek Tasarım</span>
      <!--<span class="tm-icon-desc">Any answer, and the reply, when made, was unpropitious.</span>-->
    </div>
    <div class="tm-fullwidth-icon">
      <span class="tm-icon-wrapper"><span class="tm-icon tm-icon-4"> </span></span>
      <span class="tm-icon-title">Sizi Arayalım</span>
      <!--<span class="tm-icon-desc">When the last &#8216;natural&#8217; had been declared, and the profit and loss.</span>-->
    </div>
    <div class="tm-fullwidth-icon">
      <span class="tm-icon-wrapper"><span class="tm-icon tm-icon-5"> </span></span>
      <span class="tm-icon-title">Beraber Çalışalım</span>
      <!--<span class="tm-icon-desc">Account of fish and sixpences adjusted, to the satisfaction of all parties.</span>-->
    </div>
  </div>
  <h4 class="uk-article-title">Yazılım Geliştirme</h4>
  <div class="uk-grid">
    <div class="uk-width-medium-1-2">
      <p><img src="/extra/uploads/2016/11/sh_about_img_1.jpg" alt="sh_about_img_1" width="546" height="185" class="alignnone size-full wp-image-284" srcset="/extra/uploads/2016/11/sh_about_img_1.jpg 546w, /extra/uploads/2016/11/sh_about_img_1-270x91.jpg 270w" sizes="(max-width: 546px) 100vw, 546px"></p>
      <p>Müşterilerimizin kuralları sürekli değişen iş ortamlarına adapte olabilmek için, günlük, hatta saatlik güncellemeler ile tüm yazılımlarımızı her an canlı tutuyoruz. Büyük ölçekli web yazılımlarından mobil uygulamalara, yüksek işlem hacimli dağıtık uygulamalardan web servislerine, geniş bir kapsamda yazılım çözümleri sunuyoruz.</p>
    </div>
    <div class="uk-width-medium-1-2">
      <p>Yazılım geliştirme tecrübelerimizi müşterilerimiz ile paylaşıyoruz.
		  Sorunların tespiti, süreç analizi, çözüme dair öneriler ve yazılım
		  geliştirme alanındaki teknik desteğimiz ile müşterilerimize yardımcı oluyoruz.
		  Özellikle büyük ölçekli ve özelliklerin çok sık değiştiği,
		  yönetilebilirliği yüksek yazılımların geliştirilmesi konusundaki
		  tecrübelerimizi son teknoloji ile harmanlayarak müşterilerimize sunuyoruz.
	  </p>
      <p><img src="/extra/uploads/2016/11/sh_about_img_2.jpg" alt="sh_about_img_2" width="546" height="185" class="alignnone size-full wp-image-285" srcset="/extra/uploads/2016/11/sh_about_img_2.jpg 546w, ..\wp-content\uploads\2016\11\sh_about_img_2-270x91.jpg 270w" sizes="(max-width: 546px) 100vw, 546px"></p>
    </div>
  </div>
</div>

    </article>


                      </main>


                  </div>

                                                                </div>
        </div>
@endsection