@extends('frontend.app')
@section('breadcrumb')
    <div class="uk-container uk-container-center uk-breadcrumbs-title">
        <div class="page-title">
            <span>İletişim</span>
        </div>
    </div>
    <div class="uk-breadcrumbs-wrapper">
        <div class="uk-container uk-container-center">
            <ul class="uk-breadcrumb">
                <li><a href="/">Anasayfa</a></li>
                <li class="uk-active">
                    <span>İletişim</span>
                </li>
            </ul>
        </div>
    </div>                  

@endsection
@section('content')
 <div class="tm-fullscreen uk-position-relative">
            <div class=" wpgmza_widget">
            <div id="wpgmza_map" style="display:block; overflow:auto; width:100%; height:500px; float:left;">
            
            </div>
        </div>        </div>
        
    
        
        
     
    
            <div class="uk-container uk-container-center with-breadcrumbs">
            <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">   
                                <div class="tm-main uk-width-medium-1-1">

                      
                                            <main class="tm-content uk-position-relative">
                              
    <article class="uk-article">

        
        
        <div class="kc_clfw"></div><div class="uk-grid tm-contact-info">
  <div class="uk-width-medium-6-10">
   
    <p>The knight, in order to follow so laudable an example, laid aside his helmet, his corslet, and the greater part of his armour.</p>
     <p><img src="/extra/uploads/2016/11/sh_contact_img.jpg" alt="sh_contact_img" width="100%" height="143" class="alignnone size-full wp-image-173"></p>
  </div>
  <div class="uk-width-medium-4-10">
    <h4 class="tm-contact-title">Adres</h4>
    <ul class="tm-contact-address-wrapper">
      <li>
        <span class="tm-contact-icon tm-icon-1"></span><span class="tm-contact-address">Nail Bilen Caddesi Dülger İş Merkezi Kat 4 No 10 – <br/>Gaziantep/Şahinbey</span>
      </li>
      <li>
        <span class="tm-contact-icon tm-icon-2"></span><span class="tm-contact-address">İş: --<br>Mobil: +90 551 439 6805</span>
      </li>
      <li>
        <span class="tm-contact-icon tm-icon-3"></span><span class="tm-contact-address">
            <a href="mailto:info@dgryazilim.com">info@dgryazilim.com</a>
        </span>
      </li>
    </ul>
  </div>
</div>
        
    </article>

    
                      </main>
                      
                      
                  </div>
                
                                                                </div>
        </div>
@endsection
@section('js')
<script type='text/javascript' src='//maps.google.com/maps/api/js?v=3.25&#038;key=AIzaSyCtoeKUexLUtOf_o7fFwBtiPB_wuACxDg4&#038;language=en'></script>
    <script type='text/javascript'>
/* <![CDATA[ */
var wpgmaps_mapid = "1";
var wpgmaps_localize = {"1":{"id":"1","map_title":"DGR Yazılım ve Bilişim","map_width":"100","map_height":"500","map_start_lat":"37.072063","map_start_lng":"37.370860","map_start_location":"37.072063, 37.370860","map_start_zoom":"19","default_marker":"0","type":"1","alignment":"1","directions_enabled":"1","styling_enabled":"0","styling_json":"","active":"0","kml":"","bicycle":"2","traffic":"2","dbox":"1","dbox_width":"100","listmarkers":"0","listmarkers_advanced":"0","filterbycat":"0","ugm_enabled":"0","ugm_category_enabled":"0","fusion":"","map_width_type":"%","map_height_type":"px","mass_marker_support":"1","ugm_access":"0","order_markers_by":"1","order_markers_choice":"2","show_user_location":"0","default_to":"","other_settings":{"store_locator_enabled":2,"store_locator_distance":2,"store_locator_bounce":1,"store_locator_query_string":"ZIP \/ Address:","wpgmza_store_locator_restrict":"","map_max_zoom":"1","transport_layer":2,"wpgmza_theme_data":"[{\"featureType\":\"landscape\",\"stylers\":[{\"saturation\":-100},{\"lightness\":65},{\"visibility\":\"on\"}]},{\"featureType\":\"poi\",\"stylers\":[{\"saturation\":-100},{\"lightness\":51},{\"visibility\":\"simplified\"}]},{\"featureType\":\"road.highway\",\"stylers\":[{\"saturation\":-100},{\"visibility\":\"simplified\"}]},{\"featureType\":\"road.arterial\",\"stylers\":[{\"saturation\":-100},{\"lightness\":30},{\"visibility\":\"on\"}]},{\"featureType\":\"road.local\",\"stylers\":[{\"saturation\":-100},{\"lightness\":40},{\"visibility\":\"on\"}]},{\"featureType\":\"transit\",\"stylers\":[{\"saturation\":-100},{\"visibility\":\"simplified\"}]},{\"featureType\":\"administrative.province\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"water\",\"elementType\":\"labels\",\"stylers\":[{\"visibility\":\"on\"},{\"lightness\":-25},{\"saturation\":-100}]},{\"featureType\":\"water\",\"elementType\":\"geometry\",\"stylers\":[{\"hue\":\"#ffff00\"},{\"lightness\":-25},{\"saturation\":-97}]}]","wpgmza_theme_selection":3}}};
var wpgmaps_localize_polygon_settings = [];
var wpgmaps_localize_polyline_settings = [];
var wpgmaps_markerurl = "\/\/w-starthub.torbara.com\/wp-content\/uploads\/wp-google-maps\/1markers.xml";
var wpgmaps_localize_marker_data = {"1":{"map_id":"1","marker_id":"1","title":"","address":"Gaziantep","desc":"","pic":"","icon":"","linkd":"","lat":"37.072063","lng":"37.370860","anim":"","retina":"0","category":"0","infoopen":"","other_data":""}};
var wpgmaps_localize_global_settings = {"wpgmza_settings_map_scroll":"yes","wpgmza_settings_map_open_marker_by":"1","wpgmza_api_version":"3.25","wpgmza_custom_css":"","wpgmza_settings_access_level":"manage_options","wpgmza_settings_marker_pull":"0"};
var wpgmaps_lang_km_away = "km away";
var wpgmaps_lang_m_away = "miles away";
/* ]]> */
</script>
<script type='text/javascript' src='/extra/plugins/wp-google-maps/js/wpgmaps.js?ver=6.4.03b'></script>
@endsection
@section('css')
    <link rel='stylesheet' id='wpgmaps-style-css' href='/extra/plugins/wp-google-maps/css/wpgmza_style.css?ver=6.4.03' type='text/css' media='1'>
<style id='wpgmaps-style-inline-css' type='text/css'>
    .wpgmza_map img{ 
        max-width:none; 
    } 
    .wpgmza_widget{ 
        overflow: auto; 
    }
    .tm-navbar{
        margin-bottom: 0 !important;
    }
</style>
@endsection