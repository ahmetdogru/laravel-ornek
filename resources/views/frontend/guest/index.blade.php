@extends('frontend.app')
@section('content')
    <div class="tm-fullscreen uk-position-relative">
        <div class=" AklareSlider">
            <div class="akslider-module">
                <div class="uk-slidenav-position" data-uk-slideshow="{height: &#039;auto&#039;, animation: &#039;scale&#039;, duration: &#039;500&#039;, autoplay: false, autoplayInterval: &#039;5000&#039;, videoautoplay: true, videomute: true, kenburns: false}">
                    <ul class="uk-slideshow uk-overlay-active">
                        <li class="uk-cover uk-height-viewport"><img src="/extra/uploads/2016/10/sh_slide_1.jpg" alt="sh_slide_1" width="1920" height="1000" class="alignnone size-full wp-image-46">
                            <div class="uk-overlay-panel uk-flex uk-flex-center uk-flex-middle">
                                <div class="tm-slide-style-2">
                                    <span class="tm-slide-medium-title" data-uk-scrollspy="{cls:'uk-animation-slide-top'}">WEB SİTE TASARIM NE İŞE YARAR?</span>
                                    <span class="tm-slide-text" data-uk-scrollspy="{cls:'uk-animation-slide-left'}">
                                        <ul style="font-size: 15px; text-align: left;">
                                            <!--<li>Öncelikle kurumsal bir kimliğe ulaşabilmenin ilk kuralı diyebiliriz.</li>-->
                                            <li>Profesyonelce hazırlanmış bir web sitesi sizin için en düşük maliyetli reklam aracıdır.Bu web siteleri hedef kitlenize ulaşabilmenin en kolay ve aynı zamanda en etkili yoludur.</li>
                                            <li>Web sitesi aynı zamanda,işletmenizin kimliğidir de denilebilir.Vizyonunuzu ve yaptığınız işi daha ayrıntılı ve akılda soru kalmadan anlatabileceğiniz en güzel yoldur.Bu sayede potansiyel müşteri üzerinde olumlu etki oluşturursunuz.</li>
                                            <!--<li>Seo uzmanlarımız sayesinde arama motorlarında sizi ilk sıraya taşıyarak, güvenilirliğiniz, tanınırlğınız ve karlılığınızda artar.</li>-->
                                            <li>DGR Yazılım olarak profesyonel web sitesi hizmetini size en uygun fiyat ve en uygun çözümü birlikte sunuyoruz...</li>
                                        </ul>
                                    </span>
                                    <span class="tm-slide-divider" data-uk-scrollspy="{cls:'uk-animation-slide-right'}"></span>
                                    <div class="tm-slide-buttons" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
                                        <button class="tm-slide-btn tm-btn-2">Teklif Al</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="uk-cover uk-height-viewport"><img src="/extra/uploads/2016/10/sh_slide_2-1.jpg" alt="sh_slide_2" width="1920" height="1000" class="alignnone size-full wp-image-57">
                            <div class="uk-overlay-panel uk-flex uk-flex-center uk-flex-middle">
                                <div class="tm-slide-style-2">
                                    <span class="tm-slide-medium-title" data-uk-scrollspy="{cls:'uk-animation-slide-top'}">HER SEKTÖRE ÖZEL ÇÖZÜMLER</span>
                                    <span class="tm-slide-text" data-uk-scrollspy="{cls:'uk-animation-slide-left'}">
                                        İşletmelerinizde size kolaylık sağlayabilecek fikirlerinizi bize sunun.Birlikte geliştirelim.
                                    </span>
                                    <span class="tm-slide-divider" data-uk-scrollspy="{cls:'uk-animation-slide-right'}"></span>
                                    <div class="tm-slide-buttons" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
                                        <button class="tm-slide-btn tm-btn-2">Teklif Al</button>
                                    </div>
                                </div>
                            </div>
                        </li>
<!--                        <li class="uk-cover uk-height-viewport"><img src="/extra/uploads/2016/10/sh_slide_3.jpg" alt="sh_slide_3" width="1920" height="1000" class="alignnone size-full wp-image-46">
                                <div class="uk-overlay-panel uk-flex uk-flex-center uk-flex-middle">
                                    <div class="tm-slide-style-2">
                                        <span class="tm-slide-medium-title" data-uk-scrollspy="{cls:'uk-animation-slide-top'}">Convert your visitors into clients</span>
                                        <span class="tm-slide-text" data-uk-scrollspy="{cls:'uk-animation-slide-left'}">Wayfarers slow-carb deep v lomo bespoke, asymmetrical bushwick heirloom fingerstache poutine fixie listicle pitchfork neutra swag. </span>
                                        <span class="tm-slide-divider" data-uk-scrollspy="{cls:'uk-animation-slide-right'}"></span>
                                        <div class="tm-slide-buttons" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
                                            <button class="tm-slide-btn tm-btn-1">Get Started</button>
                                            <button class="tm-slide-btn tm-btn-2">How it work</button>
                                        </div>
                                    </div>
                                </div></li>-->
                    </ul>

                    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
                    <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-text-center">
                        <li data-uk-slideshow-item="0"><a href="">0</a></li>
                        <li data-uk-slideshow-item="1"><a href="">1</a></li>
                        <!--<li data-uk-slideshow-item="2"><a href="">2</a></li>-->
                    </ul>
                </div>
            </div>

        </div>
    </div>

    <section id="tm-top-a" class="tm-top-a uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
        <div class="uk-width-1-1"><div class="uk-panel widget_text"><div class="tm-works-blocks-wrapper uk-text-center">
                    <div class="uk-container uk-container-center" data-uk-scrollspy="{cls:'uk-animation-scale-up'}">
                        <div class="uk-grid uk-grid-large">
                            <div class="tm-works-block uk-width-medium-1-3">
                                <span class="icon icon-1"></span>
                                <h4 class="skincolor">İhtiyaca Yönelik Çalışma</h4>
                                <p>Sizi dinliyor,sizi anlıyor ve ihtiyacınız olan en güzel çalışmayı, en uygun fiyata size sunuyoruz.</p>
                            </div>
                            <div class="tm-works-block uk-width-medium-1-3">
                                <span class="icon icon-2"></span>
                                <h4 class="skincolor">Özel destek</h4>
                                <p>7/24 Canlı desteğin yanında,danışmanlık hizmetimizle özel destek sunuyoruz.</p>
                            </div>
                            <div class="tm-works-block uk-width-medium-1-3">
                                <span class="icon icon-3"></span>
                                <h4 class="skincolor">İnanılmaz Basit kullanım</h4>
                                <p>Sizler için, yapacağınız işi , en kolay şekilde yapabilmenizi sağlayacak hizmetler sunuyoruz.</p>
                            </div>
                        </div>
                    </div>
                </div></div></div>
    </section>

<!--    <section id="tm-top-b" class="tm-top-b uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
        <div class="uk-width-1-1"><div class="uk-panel widget_text"><div class="tm-about-block">
                    <div class="uk-container uk-container-center">
                        <div class="tm-block-title">
                            <h3>About project</h3>
                        </div>
                        <div class="uk-grid">
                            <div class="uk-width-medium-1-2 tm-about-img" data-uk-scrollspy="{cls:'uk-animation-slide-left'}">
                                <img src="/extra/uploads/2016/10/sh_about_img.jpg" alt="sh_about_img" width="630" height="522" class="alignnone size-full wp-image-17">
                            </div>
                            <div class="uk-width-medium-1-2 tm-about-text-wrapper" data-uk-scrollspy="{cls:'uk-animation-slide-right'}">
                                <p class="tm-about-subtitle">This sounded a very good reason, and Alice was quite pleased to know it. 'I never thought of that before!' she said.</p>
                                <h4 class="tm-about-title">letterpress church-ke</h4>
                                <div class="tm-about-text">
                                    <span>Tote bag bushwick retro, fanny pack irony kitsch</span>
                                    <p>It wouldn't do?' thought Alice, as she groped her way among the tables and chairs, for the shop was very dark towards the end.</p>
                                    <span>Drift down the stream as it would</span>
                                    <p>But, before she put him on the table, she thought she might as well dust him a little, he was so covered with ashes.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div></div></div>
    </section>-->

    <section id="tm-top-c" class="tm-top-c uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="" style="text-align: center;">
        <div class="uk-width-1-1"><div class="uk-panel widget_text"><div class="tm-counters-blocks">
                    <div class="uk-container uk-container-center">
                        <div class="uk-grid uk-text-center">
<!--                            <div class="tm-counter-wrapper uk-width-medium-1-5">
                                <span class="icon icon-1">&nbsp;</span>
                                <h4 class="tm-counter">820</h4>
                                <span class="tm-counter-label">Tamamlanmış projeler
    </span>
                            </div>-->
                            <div class="tm-counter-wrapper uk-width-medium-1-3">
                                <span class="icon icon-2">&nbsp;</span>
                                <h4 class="tm-counter">3500</h4>
                                <span class="tm-counter-label">Satır Kod</span>
                            </div>
                            <div class="tm-counter-wrapper uk-width-medium-1-3">
                                <span class="icon icon-3">&nbsp;</span>
                                <h4 class="tm-counter">25450</h4>
                                <span class="tm-counter-label">Toplam İndirme</span>
                            </div>
<!--                            <div class="tm-counter-wrapper uk-width-medium-1-5">
                                <span class="icon icon-4">&nbsp;</span>
                                <h4 class="tm-counter">58</h4>
                                <span class="tm-counter-label">Bizim ortaklarımız
    </span>
                            </div>-->
                            <div class="tm-counter-wrapper uk-width-medium-1-3">
                                <span class="icon icon-5">&nbsp;</span>
                                <h4 class="tm-counter">6300</h4>
                                <span class="tm-counter-label">Bir fincan kahve
    </span>
                            </div>
                        </div>
                    </div>
                </div></div></div>
    </section>

<!--    <section id="tm-top-d" class="tm-top-d uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
        <div class="uk-width-1-1"><div class="uk-panel widget_text"><div class="tm-pricing-blocks-wrapper">
                    <div class="uk-container uk-container-center">
                        <div class="tm-block-title" data-uk-scrollspy="{cls:'uk-animation-scale-up'}">
                            <h3>Product Pricing</h3>
                            <h4 class="tm-subtitle">Chia mumblecore aesthetic listicle, try-hard bitters vice leggings put a bird on it.</h4>
                        </div>
                        <div class="uk-grid uk-grid-medium uk-text-center">
                            <div class="uk-width-medium-1-4 tm-pricing-box" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
                                <div class="uk-panel uk-panel-hover tm-pricing-box-inner">
                                    <span class="tm-pricing-label skincolor">Starter</span>
                                    <span class="tm-pricing-price">free</span>
                                    <span class="tm-pricing-desc">Butcher farm-to-table<br> Denim bitters listicle tofuman<br> Truffaut tousled DIY<br> Iumbersexual gastropub</span>
                                    <button class="uk-button" type="button">Order</button>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-4 tm-pricing-box" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
                                <div class="uk-panel uk-panel-hover tm-pricing-box-inner">
                                    <span class="tm-pricing-label skincolor">Advanced</span>
                                    <span class="tm-pricing-price">$10<strong>/per month</strong></span>
                                    <span class="tm-pricing-desc">Denim bitters listicle tofuman<br> Truffaut tousled DIY<br> Iumbersexual gastropub<br> Affogato scenester kogi</span>
                                    <button class="uk-button" type="button">Order</button>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-4 tm-pricing-box" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
                                <div class="uk-panel uk-panel-hover tm-pricing-box-inner">
                                    <span class="tm-pricing-label skincolor">Ultimate</span>
                                    <span class="tm-pricing-price">$19<strong>/per month</strong></span>
                                    <span class="tm-pricing-desc">Green juice cronut, retro<br> Butcher farm-to-table<br> Denim bitters listicle tofuman<br> Truffaut tousled DIY</span>
                                    <button class="uk-button" type="button">Order</button>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-4 tm-pricing-box" data-uk-scrollspy="{cls:'uk-animation-slide-bottom'}">
                                <div class="uk-panel uk-panel-hover tm-pricing-box-inner">
                                    <span class="tm-pricing-label skincolor">Diamond</span>
                                    <span class="tm-pricing-price">$29<strong>/per month</strong></span>
                                    <span class="tm-pricing-desc">Butcher farm-to-table<br> Denim bitters listicle tofuman<br> Truffaut tousled DIY<br> Iumbersexual gastropub</span>
                                    <button class="uk-button" type="button">Order</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div></div></div>
    </section>-->

    <section id="tm-top-e" class="tm-top-e uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
        <div class="uk-width-1-1"><div class="uk-panel widget_mc4wp_form_widget"><script type="text/javascript">(function() {
                            if (!window.mc4wp) {
                            window.mc4wp = {
                            listeners: [],
                                    forms    : {
                                    on: function (event, callback) {
                                    window.mc4wp.listeners.push({
                                    event   : event,
                                            callback: callback
                                    });
                                    }
                                    }
                            }
                            }
                            })();</script><!-- MailChimp for WordPress v4.1.0 - https://wordpress.org/plugins/mailchimp-for-wp/ --><form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-405" method="post" data-id="405" data-name="Subscribe Form"><div class="mc4wp-form-fields"><div class="tm-subscribe-wrapper">
                            <div class="uk-container uk-container-center">
                                <div class="es-caption"><h3>SİZE ULAŞALIM</h3>Fikirlerinizi anlatın beraber geliştirelim</div>

                                <div class="tm-subscribe-form uk-clearfix">
                                    <div class="es-textbox">
                                        <input type="text" class="tm-input-text" name="FNAME" placeholder="Adınız">
                                    </div>
                                    <div class="es-textbox">
                                        <input type="email" class="tm-input-text" name="TELEFON" placeholder="Telefon*" required="">
                                    </div>

                                    <div class="es-button">
                                        <input type="submit" class="uk-button uk-button-primary" value="Size Ulaşalım">
                                    </div>
                                </div>
                            </div>
                        </div><div style="display: none;"><input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off"></div><input type="hidden" name="_mc4wp_timestamp" value="1533718788"><input type="hidden" name="_mc4wp_form_id" value="405"><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1"></div><div class="mc4wp-response"></div></form><!-- / MailChimp for WordPress Plugin --></div></div>
                                    </section>

    <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
            <div class="tm-main uk-width-medium-1-1">
                <main class="tm-content uk-position-relative">
                    <article class="uk-article">
                        <div class="kc_clfw"></div><div class="tm-post-carousel-wrapper">
                            <div class="tm-block-title" data-uk-scrollspy="{cls:&#039;uk-animation-scale-up&#039;, repeat: true}">
                                <h3>Yakın Zamanda Gönderilenler</h3>
                                <!--<h4 class="tm-subtitle">Chia mumblecore aesthetic listicle, try-hard bitters vice leggings put a bird on it.</h4>-->
                            </div>
                            <div class="kc-carousel-post kc-elm kc-css-645991">
                                <div class="kc-owl-post-carousel owl-carousel list-post category " data-owl-options='{"items":"2","speed":500,"navigation":"","pagination":"yes","autoheight":"","autoplay":"yes"}'>
                                    <div class="item list-item post-98">
                                        <div class="post-content">
                                            <div class="uk-article-meta">
                                                <span class="tm-post-date">
                                                    <span class="date">
                                                        <time class="entry-date" datetime="2018-09-04T21:08:48+00:00">4 Eylül 2018</time>
                                                    </span>
                                                </span>
                                                <span class="tm-post-author">Yazar <a href="/" title="DGR Yazılım">DGR Yazılım</a></span>
                                            </div>
                                            <div class="tm-post-image">
                                                <img width="547" height="230" src="/extra/uploads/2016/11/sh_blog_img_5.jpg" class="attachment-full size-full wp-post-image" alt="" srcset="/extra/uploads/2016/11/sh_blog_img_5.jpg 547w, /extra/uploads/2016/11/sh_blog_img_5-270x114.jpg 270w" sizes="(max-width: 547px) 100vw, 547px"/>
                                            </div>
                                            <h3 class="tm-post-title">
                                                <a href="#">Yapay Zeka Siber Güvenlikte Sihirli Değnek mi?</a>
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="item list-item post-101">
                                        <div class="post-content">
                                            <div class="uk-article-meta">
                                                <span class="tm-post-date">
                                                    <span class="date">
                                                        <time class="entry-date" datetime="2018-09-04T21:08:48+00:00">4 Eylül 2018</time>
                                                    </span>
                                                </span>
                                                <span class="tm-post-author">Yazar <a href="/" title="DGR Yazılım">DGR Yazılım</a></span>
                                            </div>
                                            <div class="tm-post-image">
                                                <img width="547" height="230" src="/extra/uploads/2016/11/sh_blog_img_6.jpg" class="attachment-full size-full wp-post-image" alt=""
                                                     srcset="/extra/uploads/2016/11/sh_blog_img_6.jpg 547w, /extra/uploads/2016/11/sh_blog_img_6-270x114.jpg 270w"
                                                     sizes="(max-width: 547px) 100vw, 547px">
                                            </div>
                                            <h3 class="tm-post-title">
                                                <a href="#">WhatsApp, Google Drive'a Nasıl Yedeklenir?</a>
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="item list-item post-104">
                                        <div class="post-content">
                                            <div class="uk-article-meta">
                                                <span class="tm-post-date">
                                                    <span class="date">
                                                        <time class="entry-date" datetime="2018-09-04T21:08:48+00:00">4 Eylül 2018</time>
                                                    </span>
                                                </span>
                                                <span class="tm-post-author">Yazar <a href="/" title="DGR Yazılım">DGR Yazılım</a></span>
                                            </div>
                                            <div class="tm-post-image">
                                                <img width="547" height="230" src="/extra/uploads/2016/11/sh_blog_img_7.jpg" class="attachment-full size-full wp-post-image" alt=""
                                                     srcset="/extra/uploads/2016/11/sh_blog_img_7.jpg 547w, /extra/uploads/2016/11/sh_blog_img_7-270x114.jpg 270w"
                                                     sizes="(max-width: 547px) 100vw, 547px">
                                            </div>
                                            <h3 class="tm-post-title">
                                                <a href="#">Chrome, daha da hızlanacak!</a>
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="item list-item post-109">

                                        <div class="post-content">
                                            <div class="uk-article-meta">
                                                <span class="tm-post-date">
                                                    <span class="date">
                                                        <time class="entry-date" datetime="2018-09-04T21:08:48+00:00">4 Eylül 2018</time>
                                                    </span>
                                                </span>
                                                <span class="tm-post-author">Yazar <a href="/" title="DGR Yazılım">DGR Yazılım</a></span>
                                            </div>
                                            <div class="tm-post-image">
                                                <img width="547" height="230" src="/extra/uploads/2016/11/sh_blog_img_8.jpg" class="attachment-full size-full wp-post-image" alt=""
                                                     srcset="/extra/uploads/2016/11/sh_blog_img_8.jpg 547w, /extra/uploads/2016/11/sh_blog_img_8-270x114.jpg 270w"
                                                     sizes="(max-width: 547px) 100vw, 547px">
                                            </div>
                                            <h3 class="tm-post-title">
                                                <a href="#">Yapay Zeka Mimarisiyle Derin Öğrenme Hızlanıyor</a>
                                            </h3>
                                        </div>
                                    </div>
<!--                                    <div class="item list-item post-112">

                                        <div class="post-content">
                                            <div class="uk-article-meta">
                                                <span class="tm-post-date"><span class="date"><time class="entry-date" datetime="2016-11-02T21:17:10+00:00">November 2, 2016</time></span></span><span class="tm-post-author">Written by <a href="author/admin/index.htm" title="John Smith">John Smith</a></span>                        </div>
                                            <div class="tm-post-image"><img width="547" height="230" src="/extra/uploads/2016/11/sh_blog_img_9.jpg" class="attachment-full size-full wp-post-image" alt="" srcset="/extra/uploads/2016/11/sh_blog_img_9.jpg 547w, /extra/uploads/2016/11/sh_blog_img_9-270x114.jpg 270w" sizes="(max-width: 547px) 100vw, 547px"></div><h3 class="tm-post-title"><a href="2016/11/02/she-clutched-the-matron-by-the-arm-and-forcing/index.htm">She clutched the matron by the arm, and forcing</a></h3>
                                        </div>

                                    </div>
                                    <div class="item list-item post-115">

                                        <div class="post-content">
                                            <div class="uk-article-meta">
                                                <span class="tm-post-date"><span class="date"><time class="entry-date" datetime="2016-11-02T21:17:51+00:00">November 2, 2016</time></span></span><span class="tm-post-author">Written by <a href="author/rebecca/index.htm" title="Rebecca Hardman">Rebecca Hardman</a></span>                        </div>
                                            <div class="tm-post-image"><img width="547" height="230" src="/extra/uploads/2016/11/sh_blog_img_10.jpg" class="attachment-full size-full wp-post-image" alt="" srcset="/extra/uploads/2016/11/sh_blog_img_10.jpg 547w, /extra/uploads/2016/11/sh_blog_img_10-270x114.jpg 270w" sizes="(max-width: 547px) 100vw, 547px"></div><h3 class="tm-post-title"><a href="2016/11/02/after-we-had-jogged-on-for-some-little-time-i-asked-the-carrier-if-he-was-going-all-the-way/index.htm">After we had jogged on for some little time, I asked the  carrier if he was going all the way</a></h3>
                                        </div>

                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </article>
                </main>
            </div>
        </div>
    </div>

    <section id="tm-bottom-a" class="tm-bottom-a uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
        <h3 class="uk-hidden">&nbsp;</h3>
        <div class="uk-width-1-1">
            <div class="uk-panel widget_text">
                <div class="tm-tweet-carousel-wrapper">
                    <div class="uk-container uk-container-center">
                        <div class="tm-block-title" data-uk-scrollspy="{cls:'uk-animation-scale-up', repeat: true}">
                            <!--<h3>&nbsp;</h3>-->
                            <!--<h4 class="tm-subtitle">Tweeseitan migas pickled <span>@torbara</span></h4>-->
                        </div>
                        <div class="kc-elm kc-css-208013 kc_shortcode kc_wrap_twitter kc_twitter_feed kc_twitter_style-2" data-cfg="eyJ1c2VybmFtZSI6IktpbmdUaGVtZSIsIm51bWJlcl90d2VldCI6IjMiLCJzaG93X3RpbWUiOiJ5ZXMiLCJzaG93X3JlcGx5IjoieWVzIiwic2hvd19yZXR3ZWV0IjoieWVzIiwic2hvd19mb2xsb3dfYnV0dG9uIjoieWVzIiwiX2lkIjoiMjA4MDEzIiwiZGlzcGxheV9zdHlsZSI6IjIiLCJ3cmFwX2NsYXNzIjoiIiwic2hvd19uYXZpZ2F0aW9uIjoiIiwiYXV0b19oZWlnaHQiOiJ5ZXMiLCJzaG93X3BhZ2luYXRpb24iOiJ5ZXMiLCJzaG93X2F2YXRhciI6InllcyIsInVzZV9hcGlfa2V5IjoiIiwiY29uc3VtZXJfa2V5IjoiIiwiY29uc3VtZXJfc2VjcmV0IjoiIiwiYWNjZXNzX3Rva2VuIjoiIiwiYWNjZXNzX3Rva2VuX3NlY3JlY3QiOiIiLCJjc3NfY3VzdG9tIjoiIiwiYW5pbWF0ZSI6IiJ9" data-owl_option="{&quot;show_navigation&quot;:&quot;&quot;,&quot;show_pagination&quot;:&quot;yes&quot;,&quot;auto_height&quot;:&quot;yes&quot;}" data-display_style="2">
                            <!--<div class="result_twitter_feed"><span>Loading...</span></div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<!--    <section id="tm-bottom-b" class="tm-bottom-b uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
        <h3 class="uk-hidden">&nbsp;</h3>
        <div class="uk-width-1-1"><div class="uk-panel kc_widget_content"><div class="kc-content-widget"><style type="text/css">@media only screen and (min-width: 1000px) and (max-width: 5000px){body.kc-css-system .kc-css-188971{width: 25%;}body.kc-css-system .kc-css-584191{width: 25%;}body.kc-css-system .kc-css-981653{width: 25%;}body.kc-css-system .kc-css-618314{width: 25%;}}.kc-css-413071 .kc_column{padding-left: 10px;padding-right: 10px;}.kc-css-413071>.kc-wrap-columns{margin-left: -10px;margin-right: -10px;width: calc(100% + 20px);}</style><section class="kc-elm kc-css-844647 kc_row"><div class="kc-row-container  kc-container"><div class="kc-wrap-columns"><div class="kc-elm kc-css-396802 kc_col-sm-12 kc_column kc_col-sm-12"><div class="kc-col-container"><div class="kc-elm kc-css-565254" style="height: 80px; clear: both; width:100%;"></div><div class="kc-elm kc-css-608986 kc_text_block"><h3 class="uk-hidden"> </h3>
                                        </div></div></div></div></div></section><section class="kc-elm kc-css-876077 kc_row"><div class="kc-row-container  kc-container  uk-container uk-container-center"><div class="kc-wrap-columns"><div class="kc-elm kc-css-297425 kc_column kc_col-sm-12"><div class="kc-col-container"><div class="kc-elm kc-css-84371 kc_text_block"><div class="tm-block-title tm-team-title" data-uk-scrollspy="{cls:&#039;uk-animation-scale-up&#039;, repeat: true}">
                                                <h3>Our Awesome Team</h3>
                                                <h4 class="tm-subtitle">Adspirate terras cornua. Pinus hanc tenent instabilis his cAdspirate terras cornua</h4>
                                            </div>
                                        </div></div></div></div></div></section><section class="kc-elm kc-css-413071 kc_row"><div class="kc-row-container  kc-container  tm-team-wrapper uk-container uk-container-center"><div class="kc-wrap-columns"><div class="kc-elm kc-css-188971 kc_col-sm-3 kc_column kc_col-sm-3"><div class="kc-col-container">
                                        <div class="kc-elm kc-css-403848 kc-animated kc-animate-eff-fadeInLeft kc-animate-delay-200 kc-team kc-team-1">

                                            <figure class="content-image"><img src="/extra/uploads/2016/10/sh_team_1.jpg" alt=""></figure><div class="content-title">Aaron Larkins</div><div class="content-subtitle">Сo-founder</div><div class="content-desc"><h3 class="uk-hidden">&nbsp;</h3></div><div class="content-socials"><a href="#" target="_blank"><i class="fa-facebook"></i></a><a href="#" target="_blank"><i class="fa-twitter"></i></a><a href="#" target="_blank"><i class="fa-google-plus"></i></a></div>
                                        </div>
                                    </div></div><div class="kc-elm kc-css-584191 kc_col-sm-3 kc_column kc_col-sm-3"><div class="kc-col-container">
                                        <div class="kc-elm kc-css-744836 kc-animated kc-animate-eff-fadeInUp kc-animate-delay-300 kc-team kc-team-1">

                                            <figure class="content-image"><img src="/extra/uploads/2016/10/sh_team_2.jpg" alt=""></figure><div class="content-title">Nicole Dyson</div><div class="content-subtitle">SEO</div><div class="content-desc"><h3 class="uk-hidden">&nbsp;</h3></div><div class="content-socials"><a href="#" target="_blank"><i class="fa-facebook"></i></a><a href="#" target="_blank"><i class="fa-twitter"></i></a><a href="#" target="_blank"><i class="fa-google-plus"></i></a></div>
                                        </div>
                                    </div></div><div class="kc-elm kc-css-981653 kc_col-sm-3 kc_column kc_col-sm-3"><div class="kc-col-container">
                                        <div class="kc-elm kc-css-526422 kc-animated kc-animate-eff-fadeInUp kc-animate-delay-400 kc-team kc-team-1">

                                            <figure class="content-image"><img src="/extra/uploads/2016/10/sh_team_3.jpg" alt=""></figure><div class="content-title">Chloe Simon</div><div class="content-subtitle">Marketer</div><div class="content-desc"><h3 class="uk-hidden">&nbsp;</h3></div><div class="content-socials"><a href="#" target="_blank"><i class="fa-facebook"></i></a><a href="#" target="_blank"><i class="fa-twitter"></i></a><a href="#" target="_blank"><i class="fa-google-plus"></i></a></div>
                                        </div>
                                    </div></div><div class="kc-elm kc-css-618314 kc_col-sm-3 kc_column kc_col-sm-3"><div class="kc-col-container">
                                        <div class="kc-elm kc-css-458687 kc-animated kc-animate-eff-fadeInLeft kc-animate-delay-500 kc-team kc-team-1">

                                            <figure class="content-image"><img src="/extra/uploads/2016/10/sh_team_4.jpg" alt=""></figure><div class="content-title">Landon Gustman</div><div class="content-subtitle">Designer</div><div class="content-desc"><h3 class="uk-hidden">&nbsp;</h3></div><div class="content-socials"><a href="#" target="_blank"><i class="fa-facebook"></i></a><a href="#" target="_blank"><i class="fa-twitter"></i></a><a href="#" target="_blank"><i class="fa-google-plus"></i></a></div>
                                        </div>
                                    </div></div></div></div></section><section class="kc-elm kc-css-25729 kc_row"><div class="kc-row-container  kc-container"><div class="kc-wrap-columns"><div class="kc-elm kc-css-863748 kc_col-sm-12 kc_column kc_col-sm-12"><div class="kc-col-container"><div class="kc-elm kc-css-864485" style="height: 80px; clear: both; width:100%;"></div><div class="kc-elm kc-css-625 kc_text_block"><h3 class="uk-hidden">&nbsp;</h3>
                                        </div></div></div></div></div></section></div></div></div>
    </section>-->
@endsection
@section('css')
@endsection