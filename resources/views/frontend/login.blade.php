
    <!-- BEGIN TOP NAVIGATION MENU -->
    <div class="top-menu">
        <ul class="nav navbar-nav pull-right">
            @if($girisYapan)
                <!-- BEGIN INBOX DROPDOWN -->
                <li class="dropdown dropdown-extended dropdown-inbox dropdown-dark" id="header_inbox_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <span class="circle">3</span>
                        <span class="corner"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>You have
                                <strong>7 New</strong> Messages</h3>
                            <a href="app_inbox.html">view all</a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img src="/metronic/assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt="">
                                        </span>
                                        <span class="subject">
                                            <span class="from"> Lisa Wong </span>
                                            <span class="time">Just Now </span>
                                        </span>
                                        <span class="message"> Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img src="/metronic/assets/layouts/layout3/img/avatar3.jpg" class="img-circle" alt="">
                                        </span>
                                        <span class="subject">
                                            <span class="from"> Richard Doe </span>
                                            <span class="time">16 mins </span>
                                        </span>
                                        <span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img src="/metronic/assets/layouts/layout3/img/avatar1.jpg" class="img-circle" alt="">
                                        </span>
                                        <span class="subject">
                                            <span class="from"> Bob Nilson </span>
                                            <span class="time">2 hrs </span>
                                        </span>
                                        <span class="message"> Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                                                        <span class="photo">
                                                                            <img src="/metronic/assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""> </span>
                                        <span class="subject">
                                                                            <span class="from"> Lisa Wong </span>
                                                                            <span class="time">40 mins </span>
                                                                        </span>
                                        <span class="message"> Vivamus sed auctor 40% nibh congue nibh... </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                                                        <span class="photo">
                                                                            <img src="/metronic/assets/layouts/layout3/img/avatar3.jpg" class="img-circle" alt=""> </span>
                                        <span class="subject">
                                                                            <span class="from"> Richard Doe </span>
                                                                            <span class="time">46 mins </span>
                                                                        </span>
                                        <span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- END INBOX DROPDOWN -->
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <li class="dropdown dropdown-user dropdown-dark">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="{{ $girisYapan->logoTamYol }}">
                        <span class="username username-hide-mobile">{{ $girisYapan->name }} {{ $girisYapan->surname }}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="staff/bilgilerim">
                                <i class="icon-user"></i> Bilgilerim </a>
                        </li>
                        <li>
                            <a href="staff/haberlerim">
                                <i class="fa fa-newspaper-o"></i> Haberlerim </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="javascript:;" onclick="CikisYap()">
                                <i class="icon-key"></i>Çıkış yap </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
            @else
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <li class="dropdown dropdown-user dropdown-dark">
                    <a class="btn blue-chambray visible-sm visible-md visible-lg btn-lg pull-right"
                    href="login">
                        <i class="fa fa-sign-in"></i> Giriş Yap
                    </a>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
            @endif
        </ul>
    </div>
    <!-- END TOP NAVIGATION MENU -->
