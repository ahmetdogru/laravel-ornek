﻿<!DOCTYPE HTML>
<html lang="en-US" dir="ltr">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-77064300-4"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-77064300-4');
        </script>

        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="DGR Yazılım ve Bilişim ,Gaziantep Yazılım, Gaziantep Bilişim, Gaziantep Kurumsal Web Sitesi, Yazılım Geliştirme" />
        <meta name="keyword" content="Gaziantep,Gaziantep Yazılım,Gaziantep Bilişim,Yazılım,Bilişim,Web Sitesi,Kurumsal Web Sitesi" />
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<title>DGR Yazılım ve Bilişim</title>
		<link rel='stylesheet' id='css-aklare-slider-css' href='/extra/plugins/aklare-slider/css/aklare-slider.css?ver=4.7.11' type='text/css' media='all'>
		<link rel='stylesheet' id='contact-form-7-css' href='/extra/plugins/contact-form-7/includes/css/styles.css?ver=4.7' type='text/css' media='all'>
		<link rel='stylesheet' id='jquery-dropdown-cart-css' href='/extra/plugins/woocommerce-dropdown-cart/css/style.css?ver=4.7.11' type='text/css' media='all'>
		<link rel='stylesheet' id='woocommerce-layout-css' href='/extra/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=2.6.14' type='text/css' media='all'>
		<link rel='stylesheet' id='woocommerce-smallscreen-css' href='/extra/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=2.6.14' type='text/css' media='only screen and (max-width: 768px)'>
		<link rel='stylesheet' id='starthub-theme-css' href='/extra/themes/start-hub/css/theme.css?ver=4.7.11' type='text/css' media='all'>
		<link rel='stylesheet' id='starthub-woocommerce-css' href='/extra/themes/start-hub/css/woocommerce.css?ver=4.7.11' type='text/css' media='all'>
		<link rel='stylesheet' id='starthub-custom-css' href='/extra/themes/start-hub/css/custom.css?ver=4.7.11' type='text/css' media='all'>
		<link rel='stylesheet' id='starthub-highlight-css' href='/extra/plugins/tt-warp7/warp/vendor/highlight/highlight.css?ver=4.7.11' type='text/css' media='all'>
		<link rel='stylesheet' id='kc-general-css' href='/extra/plugins/kingcomposer/assets/frontend/css/kingcomposer.min.css?ver=2.6.11' type='text/css' media='all'>
		<link rel='stylesheet' id='kc-animate-css' href='/extra/plugins/kingcomposer/assets/css/animate.css?ver=2.6.11' type='text/css' media='all'>
		<link rel='stylesheet' id='kc-icon-1-css' href='/extra/plugins/kingcomposer/assets/css/icons.css?ver=2.6.11' type='text/css' media='all'>
		<link rel='stylesheet' id='owl-theme-css' href='/extra/plugins/kingcomposer/includes/frontend/vendors/owl-carousel/owl.theme.css?ver=2.6.11' type='text/css' media='all'>
		<link rel='stylesheet' id='owl-carousel-css' href='/extra/plugins/kingcomposer/includes/frontend/vendors/owl-carousel/owl.carousel.css?ver=2.6.11' type='text/css' media='all'>
		<link href="/js/other/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="/js/other/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<link href="/js/other/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
		<!--<link href="/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />-->
		<link href="/css/components-rounded.min.css" rel="stylesheet" type="text/css" />

		<script type='text/javascript' src='/js/jquery/jquery.min.js'></script>
		<script type='text/javascript' src='/js/jquery/jquery-migrate.min.js'></script>
		<script type='text/javascript' src='/extra/plugins/woocommerce-dropdown-cart/js/main.js?ver=4.7.11'></script>

		<link rel="icon" href="/extra/uploads/2016/11/cropped-favicon-45x45.png" sizes="32x32">
		<link rel="icon" href="/extra/uploads/2016/11/cropped-favicon-270x270.png" sizes="192x192">
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		@yield('css')
		<!-- END PAGE LEVEL PLUGINS -->
		<link rel="shortcut icon" href="favicon.ico" />
	</head>
<body class="home page-template-default page page-id-4 tm-isblog wp-front_page wp-page wp-page-4 kc-css-system">
<!--    <div class="tt-preloader tt-preloader-default">
        <div class="tt-spinner tt-preloader-box">
            <div class="tt-logo"></div>
            <p>yükleniyor...</p>
            <div class="tt-circle-1"></div>
            <div class="tt-circle-2"></div>
        </div>
    </div>-->
<div class="loading" style="background-color: black;"><div class="loading-center"><div class="loading-center-absolute"><div class="loading-object animate4"><div class="object"></div><div class="object"></div><div class="object-logo"></div></div><div class="loading-text">Yükleniyor...<small>Lütfen bekleyiniz</small></div></div></div></div>
    <header class="tm-header @if (Request::segment(1) == '') tm-transparent-header @endif">
        <div class="tm-header-top">
            <div class="uk-container uk-container-center">
                <div class="tm-headerbar-wrapper uk-float-left">
                    <div class="tm-headerbar-info">
						<div class="uk-panel widget_text">
							<ul class="tm-site-contacts">
                                <li class="tm-label skincolor"> Herhangi bir sorunuz var mı?</li>
                                <li class="tm-phone"><a href="skype:+51125541558?call" title="+90 551 439 6805">+90 551 439 6805</a></li>
                                <li class="tm-email"><a href="mailto:info@dgryazilim.com" title="info@dgryazilim.com">info@dgryazilim.com</a></li>
                            </ul>
						</div>
					</div>
                </div>
                <div class="uk-navbar-flip">
                    <div class="uk-navbar-content"><div class="tm-search-wrapper">
                            <div class="tm-search-btn"></div>
                            <div class="tm-search-inner">
                                <span class="tm-search-close-btn">
                                    <i class="uk-icon-times"></i>
                                </span>
                                <form class="uk-search">
                                    <input class="uk-search-field" type="text" placeholder="Ara">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="tm-navbar uk-navbar">
            <div class="tm-header-bottom" data-uk-sticky="{top:-100, animation: 'uk-animation-slide-top'}">
                <div class="uk-container uk-container-center">
                    <div class="tm-header-bottom-left with-socials" style="padding: 20px 40px 20px 40px;">
                        <a href="/" style="opacity: 1;">
							<img src="extra/themes/start-hub/images/dgrlogo.png" style="width: 150px; height: 55px;"/>
						</a>
                        <a href="#offcanvas" class="uk-navbar-toggle uk-visible-medium uk-visible-small" data-uk-offcanvas=""></a>
                        <ul class="uk-navbar-nav uk-hidden-medium uk-hidden-small" style="margin-top: 10px;">
                            <li @if (Request::segment(1) == '') class="uk-active" @endif><a href="/" class="">Anasayfa</a></li>
<!--                            <li class="uk-parent" data-uk-dropdown="{'preventflip':'y'}" aria-haspopup="true" aria-expanded="false">
                                <a href="category/blog/index.htm" class="">Blog</a>
                                <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-1">
                                    <div class="uk-grid uk-dropdown-grid">
                                        <div class="uk-width-1-1">
                                            <ul class="uk-nav uk-nav-navbar">
                                                <li>
                                                    <a href="category/blog/2-columns/index.htm" class="">2 columns</a>
                                                </li>
                                                <li>
                                                    <a href="category/blog/blog-sidebar/index.htm" class="">Blog Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="2016/10/27/single-post/index.htm" class="">Single Post</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>-->
                            <li @if (Request::segment(1) == 'hakkimizda') class="uk-active" @endif>
                                <a href="hakkimizda" class="">Hakkımızda</a>
                            </li>
                            <li>
                                <a href="contact/index.htm" class="">Referanslar</a>
                            </li>
                            <li @if (Request::segment(1) == 'iletisim') class="uk-active" @endif>
                                <a href="iletisim" class="">İletişim</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            @yield('breadcrumb')
        </nav>
    </header>
    <!-- END HEAD -->
     @yield('content')
    <section id="tm-bottom-d" class="tm-bottom-d uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
        <h3 class="uk-hidden">&nbsp;</h3>
        <div class="uk-width-1-1">
            <div class="uk-panel widget_text">
                <div class="tm-contact-form-wrapper">
                    <div class="uk-container uk-container-center">
                        <div class="tm-block-title">
                            <h3>Bizimle iletişime geçin</h3>
                            <!--<h4 class="tm-subtitle">Adspirate terras cornua. Pinus hanc tenent instabilis his Adspirate terras Wanna Discuss with anything ?</h4>-->
                        </div>
                        <div>
                            <div class="screen-reader-response"></div>
                            <div class="uk-form uk-form-horizontal tm-contact-form">
                                <div class="uk-grid uk-grid-collapse">
                                    <div class="uk-width-1-2">
                                        <span class="wpcf7-form-control-wrap tm-contact-name">
                                            <input type="text" id="ziyaretci_ad" size="40" placeholder="Adınız"/>
                                        </span>
                                    </div>
                                    <div class="uk-width-1-2">
                                        <span class="wpcf7-form-control-wrap tm-contact-email">
                                            <input type="text" id="ziyaretci_email" style="width: 100%;" size="40" placeholder="Email"/>
                                        </span>
                                    </div>
                                    <div class="uk-width-1-1">
                                        <span class="wpcf7-form-control-wrap tm-contact-message">
                                            <textarea cols="40" id="ziyaretci_mesaj" style="width: 100%;" rows="10" placeholder="Mesajınız"></textarea>
                                        </span>
                                    </div>
                                    <div class="tm-contact-button uk-text-center uk-width-1-1">
                                        <button class="btn btn-danger btn-lg margin-top-10" onclick="IletisimeGec();">Gönder</button>
                                    </div>
                                </div>
                            </div>
                            <div class="wpcf7-response-output wpcf7-display-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


                                                                                    <footer id="tm-footer" class="tm-footer">
                                                                                        <a class="tm-totop-scroller" data-uk-smooth-scroll="" href="#"></a>
                                                                                        <div class="tm-footer-top-block">
                                                                                        <div class="uk-container uk-container-center">
                                                                                        <div class="uk-grid uk-grid-large">

                                                                                        <div class="uk-width-1-1 uk-width-medium-1-3"><div class="uk-panel widget_text"><div class="tm-site-info">
                                                                                        <div class="tm-logo-wrapper">
                                                                                        <img src="extra/themes/start-hub/images/dgrlogo.png" style="width: 150px; height: 55px;"/>
                                                                                        </div>
                                                                                        <p class="tm-site-desc" style="color: white;">İncilipınar Mahallesi Nail Bilen Caddesi <br/>Dülger İş Merkezi Kat 4 No 10<br/> Şehitkamil/Gaziantep</p>
                                                                                        <ul class="tm-site-contacts">
                                                                                        <li class="tm-phone"><a href="skype:+905514396805?call" title="+90 551 439 6805">+90 551 439 6805</a></li>
                                                                                        <li class="tm-email"><a href="mailto:startHub@gmail.com" title="info@dgryazilim.com">info@dgryazilim.com</a></li>
                                                                                        </ul>
                                                                                        </div></div></div>

                                                                                        <div class="uk-width-1-1 uk-width-medium-1-3">
                                                                                        </div>

                                                                                        <div class="uk-width-1-1 uk-width-medium-1-3"><div class="uk-panel StarthubPortfolio"><h4 class="tm-footer-widget-title">Portfolio</h4>

                                                                                        <div class="tm-portfolio-widget">
                                                                                        <div data-uk-grid="{gutter: 8}">

                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/10/tote-bag-bushwick-retro-fanny-pack-irony-kitsch/index.htm" title="Tote bag bushwick retro, fanny pack irony kitsch"><img src="/extra/uploads/2016/11/sh_portfolio_img_1.jpg" alt="Tote bag bushwick retro, fanny pack irony kitsch"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/10/single-project/index.htm" title="Single project"><img src="/extra/uploads/2016/11/sh_portfolio_img_3.jpg" alt="Single project"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/10/sit-ea-alterum-apeirian/index.htm" title="Sit ea alterum apeirian"><img src="/extra/uploads/2016/11/sh_portfolio_img_4.jpg" alt="Sit ea alterum apeirian"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/10/eam-an-assum-congue/index.htm" title="Eam an assum congue"><img src="/extra/uploads/2016/11/sh_portfolio_img_6.jpg" alt="Eam an assum congue"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/10/nihil-definiebas-nam-ne/index.htm" title="Nihil definiebas nam ne"><img src="/extra/uploads/2016/11/sh_portfolio_img_8.jpg" alt="Nihil definiebas nam ne"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/12/sed-ei-dolor-efficiendi/index.htm" title="Sed ei dolor efficiendi"><img src="/extra/uploads/2016/11/sh_portfolio_img_10.jpg" alt="Sed ei dolor efficiendi"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/12/an-semper-luptatum-eam/index.htm" title="An semper luptatum eam"><img src="/extra/uploads/2016/11/sh_portfolio_img_12.jpg" alt="An semper luptatum eam"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/12/ei-mea-magna-errem-aliquando/index.htm" title="Ei mea magna errem aliquando"><img src="/extra/uploads/2016/11/sh_portfolio_img_14.jpg" alt="Ei mea magna errem aliquando"></a>
                                                                                        </div>

                                                                                        </div>
                                                                                        </div>

                                                                                        </div></div>
                                                                                        </div>
                                                                                        </div>
                                                                                        </div>
                                                                                        <div class="tm-footer-bottom-block">
                                                                                        <div class="uk-container uk-container-center">
                                                                                        <div class="uk-grid uk-grid-large">

                                                                                        <!--<div class="uk-width-1-1 uk-width-medium-1-2"><div class="uk-panel widget_nav_menu"><ul class="uk-subnav uk-subnav-line"><li class="uk-active"><a href="index.htm" class="">Home</a></li><li><a href="about-us/index.htm" class="">About</a></li><li><a href="category/blog/index.htm" class="">Blog</a></li><li><a href="category/portfolio/index.htm" class="">Works</a></li><li><a href="shop/index.htm" class="">Store</a></li><li><a href="contact/index.htm" class="">Contact</a></li></ul></div></div>-->

                                                                                        <div class="uk-width-1-1 uk-width-medium-1-2"><div class="uk-panel widget_text"><address class="tm-copyright">Created by <a href="/" title="DGR Yazılım ve Bilişim">DGR YAZILIM VE BİLİŞİM</a></address></div></div>
                                                                                        </div>
                                                                                        </div>
                                                                                        </div>

                                                                                    </footer>
	<script src="/js/other/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/js/other/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
	<script src="/js/other/bootstrap-sweetalert/sweetalert.js" type="text/javascript"></script>
                                                                                    <script type="text/javascript">(function() {function addEventListener(element, event, handler) {
                                                                                        if (element.addEventListener) {
                                                                                        element.addEventListener(event, handler, false);
                                                                                        } else if (element.attachEvent){
                                                                                        element.attachEvent('on' + event, handler);
                                                                                        }
                                                                                        }function maybePrefixUrlField() {
                                                                                        if (this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
                                                                                        this.value = "http://" + this.value;
                                                                                        }
                                                                                        }

                                                                                        var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
                                                                                        if (urlFields && urlFields.length > 0) {
                                                                                        for (var j = 0; j < urlFields.length; j++) {
                                                                                        addEventListener(urlFields[j], 'blur', maybePrefixUrlField);
                                                                                        }
                                                                                        }/* test if browser supports date fields */
                                                                                        var testInput = document.createElement('input');
                                                                                        testInput.setAttribute('type', 'date');
                                                                                        if (testInput.type !== 'date') {

                                                                                        /* add placeholder & pattern to all date fields */
                                                                                        var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
                                                                                        for (var i = 0; i < dateFields.length; i++) {
                                                                                        if (!dateFields[i].placeholder) {
                                                                                        dateFields[i].placeholder = 'YYYY-MM-DD';
                                                                                        }
                                                                                        if (!dateFields[i].pattern) {
                                                                                        dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
                                                                                        }
                                                                                        }
                                                                                        }

                                                                                        })();</script>
	<script type='text/javascript' src='/extra/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
	<script type='text/javascript' src='/extra/plugins/contact-form-7/includes/js/scripts.js?ver=4.7'></script>
	<script type='text/javascript' src='/extra/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=2.6.14'></script>
	<script type='text/javascript' src='/extra/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
	<script type='text/javascript' src='/extra/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=2.6.14'></script>
	<script type='text/javascript' src='/extra/plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min.js?ver=1.4.1'></script>
	<script type='text/javascript' src='/extra/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=2.6.14'></script>
	<script type='text/javascript' src='/js/comment-reply.min.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/uikit.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/autocomplete.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/search.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/grid.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/slideshow.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/slideshow-fx.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/sticky.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/lightbox.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/themes/start-hub/js/jquery.countdown.min.js?ver=4.7.11'></script>
	<!--<script type='text/javascript' src='/extra/themes/start-hub/js/comments.js?ver=4.7.11'></script>-->
	<script type='text/javascript' src='/extra/themes/start-hub/js/theme.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/kingcomposer/assets/frontend/js/kingcomposer.min.js?ver=2.6.11'></script>
	<script type='text/javascript' src='/extra/plugins/kingcomposer/includes/frontend/vendors/owl-carousel/owl.carousel.min.js?ver=2.6.11'></script>

	<script src="/js/cpaint/cpaint2.inc.js" type="text/javascript"></script>

	<!--<script type='text/javascript' src='/js/wp-embed.min.js?ver=4.7.11'></script>-->
	<!--<script type='text/javascript' src='/extra/plugins/mailchimp-for-wp/assets/js/forms-api.min.js?ver=4.1.0'></script>-->

	<div id="offcanvas" class="uk-offcanvas">
		<div class="uk-offcanvas-bar">
			<ul class="uk-nav uk-nav-offcanvas">
				<li class="uk-active">
					<a href="/" class="">Anasayfa</a>
				</li>
				<li>
					<a href="hakkimizda" class="">Hakkımızda</a>
				</li>
<!--				<li class="uk-parent">
					<a href="category/blog/index.htm" class="">Blog</a>
					<ul class="uk-nav-sub">
						<li>
							<a href="category/blog/2-columns/index.htm" class="">2 columns</a>
						</li>
						<li>
							<a href="category/blog/blog-sidebar/index.htm" class="">Blog Sidebar</a>
						</li>
						<li>
							<a href="2016/10/27/single-post/index.htm" class="">Single Post</a>
						</li>
					</ul>
				</li>-->
				<li>
					<a href="referanslar" class="">Referanslar</a>
				</li>
				<li>
					<a href="iletisim" class="">İletişim</a>
				</li>
			</ul>
		</div>
	</div>

	<script type="text/javascript">(function() {function addEventListener(element, event, handler) {
		if (element.addEventListener) {
		element.addEventListener(event, handler, false);
		} else if (element.attachEvent){
		element.attachEvent('on' + event, handler);
		}
		}function maybePrefixUrlField() {
		if (this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
		this.value = "http://" + this.value;
		}
		}

		var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
		if (urlFields && urlFields.length > 0) {
		for (var j = 0; j < urlFields.length; j++) {
		addEventListener(urlFields[j], 'blur', maybePrefixUrlField);
		}
		}/* test if browser supports date fields */
		var testInput = document.createElement('input');
		testInput.setAttribute('type', 'date');
		if (testInput.type !== 'date') {

		/* add placeholder & pattern to all date fields */
		var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
		for (var i = 0; i < dateFields.length; i++) {
		if (!dateFields[i].placeholder) {
		dateFields[i].placeholder = 'YYYY-MM-DD';
		}
		if (!dateFields[i].pattern) {
		dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
		}
		}
		}

		})();
	</script>
	@yield('js')
	<script src="/js/common.js" type="text/javascript"></script>
                                                                                    <div class="tm-block tm-block-social">
                                                                                        <div class="uk-grid uk-grid-collapse">
                                                                                        <div class="uk-width-1-5"><a href="https://www.facebook.com/dgryazilim/" class="tm-social-link facebook" target="_blank">Facebook</a></div>
                                                                                        <div class="uk-width-1-5"><a href="https://twitter.com/dgryazilim" class="tm-social-link twitter" target="_blank">Twitter</a></div>
                                                                                        <div class="uk-width-1-5"><a href="https://www.instagram.com/dgryazilim/" class="tm-social-link instagram" target="_blank">Instagram</a></div>
                                                                                        <div class="uk-width-1-5"><a href="https://www.linkedin.com/company/dgryazilim" class="tm-social-link linkedin" target="_blank">Linkedin</a></div>
                                                                                        <div class="uk-width-1-5"><a href="#" class="tm-social-link youtube" target="_blank">Youtube</a></div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <footer id="tm-footer" class="tm-footer">
                                                                                        <a class="tm-totop-scroller" data-uk-smooth-scroll="" href="#"></a>
                                                                                        <div class="tm-footer-top-block">
                                                                                        <div class="uk-container uk-container-center">
                                                                                        <div class="uk-grid uk-grid-large">

                                                                                        <div class="uk-width-1-1 uk-width-medium-1-3"><div class="uk-panel widget_text"><div class="tm-site-info">
                                                                                        <div class="tm-logo-wrapper">
                                                                                        <img src="extra/themes/start-hub/images/dgrlogo.png" style="width: 150px; height: 55px;"/>
                                                                                        </div>
                                                                                        <p class="tm-site-desc" style="color: white;">İncilipınar Mahallesi Nail Bilen Caddesi <br/>Dülger İş Merkezi Kat 4 No 10<br/> Şehitkamil/Gaziantep</p>
                                                                                        <ul class="tm-site-contacts">
                                                                                        <li class="tm-phone"><a href="skype:+905514396805?call" title="+90 551 439 6805">+90 551 439 6805</a></li>
                                                                                        <li class="tm-email"><a href="mailto:startHub@gmail.com" title="info@dgryazilim.com">info@dgryazilim.com</a></li>
                                                                                        </ul>
                                                                                        </div></div></div>

                                                                                        <div class="uk-width-1-1 uk-width-medium-1-3">
                                                                                        </div>

                                                                                        <div class="uk-width-1-1 uk-width-medium-1-3"><div class="uk-panel StarthubPortfolio"><h4 class="tm-footer-widget-title">Portfolio</h4>

                                                                                        <div class="tm-portfolio-widget">
                                                                                        <div data-uk-grid="{gutter: 8}">

                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/10/tote-bag-bushwick-retro-fanny-pack-irony-kitsch/index.htm" title="Tote bag bushwick retro, fanny pack irony kitsch"><img src="/extra/uploads/2016/11/sh_portfolio_img_1.jpg" alt="Tote bag bushwick retro, fanny pack irony kitsch"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/10/single-project/index.htm" title="Single project"><img src="/extra/uploads/2016/11/sh_portfolio_img_3.jpg" alt="Single project"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/10/sit-ea-alterum-apeirian/index.htm" title="Sit ea alterum apeirian"><img src="/extra/uploads/2016/11/sh_portfolio_img_4.jpg" alt="Sit ea alterum apeirian"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/10/eam-an-assum-congue/index.htm" title="Eam an assum congue"><img src="/extra/uploads/2016/11/sh_portfolio_img_6.jpg" alt="Eam an assum congue"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/10/nihil-definiebas-nam-ne/index.htm" title="Nihil definiebas nam ne"><img src="/extra/uploads/2016/11/sh_portfolio_img_8.jpg" alt="Nihil definiebas nam ne"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/12/sed-ei-dolor-efficiendi/index.htm" title="Sed ei dolor efficiendi"><img src="/extra/uploads/2016/11/sh_portfolio_img_10.jpg" alt="Sed ei dolor efficiendi"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/12/an-semper-luptatum-eam/index.htm" title="An semper luptatum eam"><img src="/extra/uploads/2016/11/sh_portfolio_img_12.jpg" alt="An semper luptatum eam"></a>
                                                                                        </div>


                                                                                        <div class="uk-width-1-4">
                                                                                        <a href="2016/11/12/ei-mea-magna-errem-aliquando/index.htm" title="Ei mea magna errem aliquando"><img src="/extra/uploads/2016/11/sh_portfolio_img_14.jpg" alt="Ei mea magna errem aliquando"></a>
                                                                                        </div>

                                                                                        </div>
                                                                                        </div>

                                                                                        </div></div>
                                                                                        </div>
                                                                                        </div>
                                                                                        </div>
                                                                                        <div class="tm-footer-bottom-block">
                                                                                        <div class="uk-container uk-container-center">
                                                                                        <div class="uk-grid uk-grid-large">

                                                                                        <!--<div class="uk-width-1-1 uk-width-medium-1-2"><div class="uk-panel widget_nav_menu"><ul class="uk-subnav uk-subnav-line"><li class="uk-active"><a href="index.htm" class="">Home</a></li><li><a href="about-us/index.htm" class="">About</a></li><li><a href="category/blog/index.htm" class="">Blog</a></li><li><a href="category/portfolio/index.htm" class="">Works</a></li><li><a href="shop/index.htm" class="">Store</a></li><li><a href="contact/index.htm" class="">Contact</a></li></ul></div></div>-->

                                                                                        <div class="uk-width-1-1 uk-width-medium-1-2"><div class="uk-panel widget_text"><address class="tm-copyright">Created by <a href="/" title="DGR Yazılım ve Bilişim">DGR YAZILIM VE BİLİŞİM</a></address></div></div>
                                                                                        </div>
                                                                                        </div>
                                                                                        </div>

                                                                                    </footer>
	<script src="/js/other/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/js/other/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
	<script src="/js/other/bootstrap-sweetalert/sweetalert.js" type="text/javascript"></script>
                                                                                    <script type="text/javascript">(function() {function addEventListener(element, event, handler) {
                                                                                        if (element.addEventListener) {
                                                                                        element.addEventListener(event, handler, false);
                                                                                        } else if (element.attachEvent){
                                                                                        element.attachEvent('on' + event, handler);
                                                                                        }
                                                                                        }function maybePrefixUrlField() {
                                                                                        if (this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
                                                                                        this.value = "http://" + this.value;
                                                                                        }
                                                                                        }

                                                                                        var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
                                                                                        if (urlFields && urlFields.length > 0) {
                                                                                        for (var j = 0; j < urlFields.length; j++) {
                                                                                        addEventListener(urlFields[j], 'blur', maybePrefixUrlField);
                                                                                        }
                                                                                        }/* test if browser supports date fields */
                                                                                        var testInput = document.createElement('input');
                                                                                        testInput.setAttribute('type', 'date');
                                                                                        if (testInput.type !== 'date') {

                                                                                        /* add placeholder & pattern to all date fields */
                                                                                        var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
                                                                                        for (var i = 0; i < dateFields.length; i++) {
                                                                                        if (!dateFields[i].placeholder) {
                                                                                        dateFields[i].placeholder = 'YYYY-MM-DD';
                                                                                        }
                                                                                        if (!dateFields[i].pattern) {
                                                                                        dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
                                                                                        }
                                                                                        }
                                                                                        }

                                                                                        })();</script>
	<script type='text/javascript' src='/extra/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
	<script type='text/javascript' src='/extra/plugins/contact-form-7/includes/js/scripts.js?ver=4.7'></script>
	<script type='text/javascript' src='/extra/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=2.6.14'></script>
	<script type='text/javascript' src='/extra/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
	<script type='text/javascript' src='/extra/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=2.6.14'></script>
	<script type='text/javascript' src='/extra/plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min.js?ver=1.4.1'></script>
	<script type='text/javascript' src='/extra/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=2.6.14'></script>
	<script type='text/javascript' src='/js/comment-reply.min.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/uikit.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/autocomplete.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/search.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/grid.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/slideshow.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/slideshow-fx.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/sticky.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/tt-warp7/warp/vendor/uikit/js/components/lightbox.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/themes/start-hub/js/jquery.countdown.min.js?ver=4.7.11'></script>
	<!--<script type='text/javascript' src='/extra/themes/start-hub/js/comments.js?ver=4.7.11'></script>-->
	<script type='text/javascript' src='/extra/themes/start-hub/js/theme.js?ver=4.7.11'></script>
	<script type='text/javascript' src='/extra/plugins/kingcomposer/assets/frontend/js/kingcomposer.min.js?ver=2.6.11'></script>
	<script type='text/javascript' src='/extra/plugins/kingcomposer/includes/frontend/vendors/owl-carousel/owl.carousel.min.js?ver=2.6.11'></script>

	<script src="/js/cpaint/cpaint2.inc.js" type="text/javascript"></script>

	<!--<script type='text/javascript' src='/js/wp-embed.min.js?ver=4.7.11'></script>-->
	<!--<script type='text/javascript' src='/extra/plugins/mailchimp-for-wp/assets/js/forms-api.min.js?ver=4.1.0'></script>-->

	<div id="offcanvas" class="uk-offcanvas">
		<div class="uk-offcanvas-bar">
			<ul class="uk-nav uk-nav-offcanvas">
				<li class="uk-active">
					<a href="/" class="">Anasayfa</a>
				</li>
				<li>
					<a href="hakkimizda" class="">Hakkımızda</a>
				</li>
<!--				<li class="uk-parent">
					<a href="category/blog/index.htm" class="">Blog</a>
					<ul class="uk-nav-sub">
						<li>
							<a href="category/blog/2-columns/index.htm" class="">2 columns</a>
						</li>
						<li>
							<a href="category/blog/blog-sidebar/index.htm" class="">Blog Sidebar</a>
						</li>
						<li>
							<a href="2016/10/27/single-post/index.htm" class="">Single Post</a>
						</li>
					</ul>
				</li>-->
				<li>
					<a href="referanslar" class="">Referanslar</a>
				</li>
				<li>
					<a href="iletisim" class="">İletişim</a>
				</li>
			</ul>
		</div>
	</div>

	<script type="text/javascript">(function() {function addEventListener(element, event, handler) {
		if (element.addEventListener) {
		element.addEventListener(event, handler, false);
		} else if (element.attachEvent){
		element.attachEvent('on' + event, handler);
		}
		}function maybePrefixUrlField() {
		if (this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
		this.value = "http://" + this.value;
		}
		}

		var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
		if (urlFields && urlFields.length > 0) {
		for (var j = 0; j < urlFields.length; j++) {
		addEventListener(urlFields[j], 'blur', maybePrefixUrlField);
		}
		}/* test if browser supports date fields */
		var testInput = document.createElement('input');
		testInput.setAttribute('type', 'date');
		if (testInput.type !== 'date') {

		/* add placeholder & pattern to all date fields */
		var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
		for (var i = 0; i < dateFields.length; i++) {
		if (!dateFields[i].placeholder) {
		dateFields[i].placeholder = 'YYYY-MM-DD';
		}
		if (!dateFields[i].pattern) {
		dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
		}
		}
		}

		})();
	</script>
	@yield('js')
	<script src="/js/common.js" type="text/javascript"></script>
        </body>
</html>
