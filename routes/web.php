<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\PageRouter;


Route::get('/',function(){
    $router = new PageRouter();
	$act  = 'guest';
	$act2 = 'index';
	$act3 = '';
	if(isset($_GET['act']))
		$act = $_GET['act'];
	if(isset($_GET['act2']))
		$act2 = $_GET['act2'];
	if(isset($_GET['act3']))
		$act3 = $_GET['act3'];
    return $router->routeWithActs(['act' => $act, 'act2' => $act2, 'act3' => $act3]);
});
Route::post('/',function(){
    $router = new PageRouter();
	$act  = 'guest';
	$act2 = 'index';
	$act3 = '';
	if(isset($_GET['act']))
		$act = $_GET['act'];
	if(isset($_GET['act2']))
		$act2 = $_GET['act2'];
	if(isset($_GET['act3']))
		$act3 = $_GET['act3'];
    return $router->routeWithActs(['act' => $act, 'act2' => $act2, 'act3' => $act3],"post");
});
if (!Request::is('admin') && !Request::is('register') && !Request::is('login'))
{
	Route::get('/{act2}',function($act2){
		$router = new PageRouter();
		return $router->routeWithActs(['act' => 'guest', 'act2' => $act2]);
	});
	Route::post('/{act2}',function($act2){
		$router = new PageRouter();
		return $router->routeWithActs(['act' => 'guest', 'act2' => $act2],"post");
	});
}

Route::post('/cikis-yap',function(){
    Auth::logout();
});

if (!Request::is('register') && !Request::is('login'))
{
    Route::group(['prefix' => 'staff', 'middleware' => 'staff'],function (){
        Route::get('/{act2?}/{act3?}', 'PageRouter@Route');
        Route::post('/{act2?}/{act3?}', 'PageRouter@Route');
    });

    Route::group(['prefix' => 'admin', 'middleware' => 'admin'],function (){
        Route::get('/{act2?}/{act3?}', 'PageRouter@Route');
        Route::post('/{act2?}/{act3?}', 'PageRouter@Route');
    });

    Route::get('/{act?}/{act2?}/{act3?}', 'PageRouter@Route');
    Route::post('/{act?}/{act2?}/{act3?}', 'PageRouter@Route');
}

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
