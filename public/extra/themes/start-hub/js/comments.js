/**
 * 
 * Start Hub - Creative Multi Concept Responsive WordPress Theme, exclusively on Envato Market: https://themeforest.net/user/torbara/portfolio?ref=torbara
 * @encoding     UTF-8
 * @copyright    Copyright (C) 2016 Torbara (http://torbara.com). All rights reserved.
 * @license      Envato Standard License http://themeforest.net/licenses/standard?ref=torbara
 * @author       Anatoly Chebotaryoff (anatolijchebotaryov@gmail.com)
 * @support      support@torbara.com
 * 
 */


jQuery(function($) {
    "use strict";
    
    var respond = $("#respond");

    $("p.js-reply > a").bind("click", function(){

        var id = $(this).attr('rel');

        respond.find(".comment-cancelReply:first").remove();

        $('<a>Cancel</a>').addClass('comment-cancelReply uk-margin-left').attr('href', "#respond").bind("click", function(){
            respond.find(".comment-cancelReply:first").remove();
            respond.appendTo($('#comments')).find("[name=comment_parent]").val(0);

            return false;
        }).appendTo(respond.find(".tm-comment-actions:first"));

        respond.find("[name=comment_parent]").val(id);
        respond.appendTo($("#comment-"+id));

        return false;

    });
});