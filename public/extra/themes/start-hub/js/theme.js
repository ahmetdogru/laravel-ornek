/**
 *
 * Start Hub - Creative Multi Concept Responsive WordPress Theme, exclusively on Envato Market: https://themeforest.net/user/torbara/portfolio?ref=torbara
 * @encoding     UTF-8
 * @copyright    Copyright (C) 2016 Torbara (http://torbara.com). All rights reserved.
 * @license      Envato Standard License http://themeforest.net/licenses/standard?ref=torbara
 * @author       Anatoly Chebotaryoff (anatolijchebotaryov@gmail.com)
 * @support      support@torbara.com
 *
 */

jQuery(function($) {
    "use strict";

    // Counter Block on frontpage
    $('.tm-counter').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });

    // Header Search
    $('.tm-header-top .tm-search-btn').off().on('click', function () {
        jQuery('.tm-header-top .tm-search-inner').addClass('active');
    });
    $('.tm-header-top .tm-search-close-btn').off().on('click', function () {
        jQuery('.tm-header-top .tm-search-inner').removeClass('active');
    });

    // Countdown for Coming soon
    jQuery("#getting-started").countdown("2017/03/01 10:33:47")
    .on('update.countdown', function(event) {
        jQuery("#getting-started .days").text(event.strftime('%D'));
        jQuery("#getting-started .hours").text(event.strftime('%H'));
        jQuery("#getting-started .minutes").text(event.strftime('%M'));
        jQuery("#getting-started .seconds").text(event.strftime('%S'));
    })
    .on('finish.countdown', function(event) {
        jQuery("#getting-started").parent().html('<h2 class="tm-grandopening-title">Grand opening!</h2>');
        //This code will be executed after the countdown
        // Add your code here
    });
    // Hide preloader
    jQuery(window).on('load', function () {
		Page.CloseProcessingMessage();
		$('DIV.loading').remove();
//        var $preloader = $(".tt-preloader");
//        var $spinner   = $preloader.find('.tt-spinner');
//        $spinner.fadeOut();
//        $preloader.delay(350).fadeOut('slow');
//
//        // Progress Bars
//        if($(".tt-preloader").length){
//            if($('.uk-progress-bar').length) {
//                $('.uk-progress-bar').each(function (){
//                   var $this = $(this);
//                   var progress_width = $this.data('width');
//                   setTimeout(function(){
//                        $this.css('width', progress_width);
//                   }, 500);
//                });
//            }
//        } else {
//            if($('.uk-progress-bar').length) {
//                $('.uk-progress-bar').each(function (){
//                   var progress_width = $(this).data('width');
//                   $(this).css('width', progress_width);
//                });
//            }
//        }

    });

    // fadeOut for page out
    jQuery(window).on('beforeunload', function(){
        jQuery("body").fadeOut('slow');
    });

});
