/* global CSRF_TOKEN, Page, */

CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
Page = {};
LIBS_URL = "";

$(function(){
	window.onbeforeunload = function(){
		Page.Loading();
	};
})

Page.CloseProcessingMessage = function () {
    $('.loading-show').removeClass('loading-show');
    $('div.loading').remove();
    var obj = $('[old_overflow]');
    obj.css('overflow', obj.attr('old_overflow'));
    obj.removeAttr('old_overflow');
    $('#DIV_ProcessingMessage').remove();
};

Page.GetWindowHref = function()
{
    var url = window.location.href;
    if (typeof ORIGINAL_URL != "undefined" && ORIGINAL_URL != '')
        url = ORIGINAL_URL;
    return url.replace(/#.*$/, '');
};

Page.UrlChangeParam = function (param, newVal, url) {
    if (typeof url == "undefined")
        url = Page.GetWindowHref();
	if(!url.match(/\?/))
		url += "?";
    var re = new RegExp("([^a-z0-9])" + param + "=([^&]+)?", "i");
    if (re.test(url))
        url = url.replace(re, "$1" + (newVal ? param + '=' + newVal : ''));
    else
        url = url + '&' + param + '=' + newVal;
    return url.replace(/&&/g, '&');
};

Page.ShowError = function (msg, okCallback)
{
    msg = Page.GetCustomMessage(msg);
    return swal( {
        title: 'Hata',
        text: msg,
        type: 'error',
        html: true,
        confirmButtonText: "Tamam"
    }, okCallback);
};

Page.ShowSuccess = function (msg, okCallback)
{
	msg = Page.GetCustomMessage(msg);
		return swal( {
			title: 'Başarılı',
			text: msg,
			type: 'success',
			confirmButtonText: "Tamam"
		}, okCallback);
};

Page.GetCustomMessage = function(msg){

    if ($.isArray(msg)){
        var args = [];
        for(var i=1; i<msg.length; i++)
            args.push(msg[i]);
        msg = vsprintf(Page.CustomMessages[msg[0]], args);
    }
    return msg;
};

Page.GetParameter = function (name, defaultVal, href, rewrite) {
	if (!is_set(defaultVal))
		defaultVal = '';
	if (!is_set(rewrite))
		rewrite=false;
	if (!href)
		return ifEmpty(Page.Params[name], defaultVal);
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(href);
	var val = '';
	if (results == null)
		val = defaultVal;
	else
		val = decodeURIComponent(results[1].replace(/\+/g, " "));
	if (! rewrite || val)
		return val;
	// rewrite olabilir ve val boş geldi
	var parts = href.split(/[\/&]/);
	var act = true;
	for(var i=0; i<parts.length; i++)
	{
		if (i == 0 && parts[i] == 'act')
		{
			act = true;
			continue;
		}
		var part = parts[i];
		var p2 = part.split('=');
		var varName = '';
		if (act && p2.length == 1)
		{
			varName = 'act' + (i == 1 ? '' : i);
			val = part;
		}
		else if (p2.length == 2)
		{
			varName = p2[0];
			val = p2[1];
		}

		if (varName == name)
			return val;
	}// for

	return defaultVal;
};

Page.CustomMessages = {};

Page.SAVING_MSG = 'Kaydediliyor...';
Page.LOADING_MSG = 'Yükleniyor...';
Page.DELETING_MSG = 'Siliniyor...';
Page.SENDING_MSG = 'Gönderiliyor...';
Page.UPDATING_MSG = 'Güncelleniyor...';

Page.Loading = function(message, progress){
    Page.ShowProcessingMessage('body', message, progress);
};

Page.Loading.Type = 3;

Page.ShowProcessingMessage = function (jqSelector, message, progress) {
    if (!message)
        message = Page.LOADING_MSG;
    var obj = $(jqSelector);
    if (obj.length == 0 || obj == null)
        obj = $(jqSelector = 'body');

    if (typeof message != 'string')
        message = Page.LOADING_MSG;
    if (obj.hasClass('loading-show'))
    {
        obj.find('.loading-text').contents().first().replaceWith(message);
        var prg = obj.find('.progress').hide();
        if (progress > 0)
        {
            prg.show();
            prg.find('.progress-bar').width(progress + '%');
            var val = prg.find('.progress-val').html(progress + '%').removeClass('half');
            if (progress >= 50)
                val.addClass('half');
        }
        return;
    }
    obj.addClass('loading-show');
    var loader = $('<div>').addClass('loading').appendTo(obj);
    var div1 = $('<div>').addClass('loading-center').appendTo(loader);
    var div2 = $('<div>').addClass('loading-center-absolute').appendTo(div1);
    var obj = $('<div>').addClass('loading-object').appendTo(div2);
    var animates = [
        function(){
            obj.addClass('animate1');
        },
        function(){
            obj.addClass('animate2');
            $('<div>').addClass('object1').appendTo(obj);
            $('<div>').addClass('object2').appendTo(obj);
            $('<div>').addClass('object-logo').appendTo(obj);
        },
        function(){
            obj.addClass('animate3');
            $('<div>').addClass('object').appendTo(obj);
            $('<div>').addClass('object').appendTo(obj);
            $('<div>').addClass('object').appendTo(obj);
            $('<div>').addClass('object').appendTo(obj);
        },
        function(){
            obj.addClass('animate4');
            $('<div>').addClass('object').appendTo(obj);
            $('<div>').addClass('object').appendTo(obj);
            $('<div>').addClass('object-logo').appendTo(obj);
        }
    ];
//	var idx = Math.round( (animates.length - 1) * Math.random() );
//	animates[idx]();
    animates[Page.Loading.Type]();
    var txtDiv = $('<div>').html(message).addClass('loading-text').appendTo(div2);
    if (typeof progress != "undefined")
    {
        div2.find('small').remove();
        var prg = $('<div>').addClass('progress').appendTo(txtDiv);
        $('<div>').addClass('progress-bar').width(progress + '%').appendTo(prg);
        var val = $('<div>').addClass('progress-val').html(progress + '%').appendTo(prg);
        if (progress >= 50)
            val.addClass('half');
    }
    else if (div2.find('small').length == 0)
        $('<small>').html('Lütfen bekleyiniz').appendTo(txtDiv);
};

Page.Ajax = function () {
    this.AutoCloseProcessingMessage = true;

    this._callback = function (resp) {
        Page.Ajax.ResponseWaiting = false;
        if (this.AutoCloseProcessingMessage && this.MessageShown)
            Page.CloseProcessingMessage();
        this.CallBackFunction(resp, this.RefreshStyle);
    };

    this.GetUrl = function () {
        var url = Page.GetWindowHref();
        return Page.UrlChangeParam('ajax', '1', url);
    };

    /**
     * @returns {Page.Ajax}
     */
    this.Send = function (ServerFunc, Params, CallBackFunc, Message, AutoCorrect) {
        if (is_set(AutoCorrect))
            this.AutoCorrect = AutoCorrect;
        if (is_set(CallBackFunc) && CallBackFunc != null)
            this.SetCallback(CallBackFunc);
        if (is_set(Message))
            this.Message = Message;
        if (Page.Ajax.ResponseWaiting && false) {
            Page.ShowError('İşleminiz devam ediyor, lütfen bekleyiniz.');
            return this;
        }
        this.MessageShown = false;
        if (this.Message != null && this.Message != '')
        {
            Page.Loading(this.Message);
            this.MessageShown = true;
        }
        Page.Ajax.ResponseWaiting = true;
        GenerateCp();
        cp.autoCorrectEncoding = this.AutoCorrect;
        cp.call(this.Url, ServerFunc, this, Params);

        return this;
    };

    /**
     * @returns {Page.Ajax}
     */
    this.SendBool = function (ServerFunc, CustomParams, CallBackFunction, Message, AutoCorrect) {
        var that = this;
        var cb = function (resp) {
            if (that.MessageShown)
                Page.CloseProcessingMessage();
            if (resp != '1')
                return Page.ShowError('Hata: ' + resp);
            if (typeof CallBackFunction == 'function')
                CallBackFunction(resp);
        };
        return this.Send(ServerFunc, CustomParams, cb, Message, AutoCorrect);
    };

    /**
     * @returns {Page.Ajax}
     */
    this.SendJson = function (ServerFunc, CustomParams, CallBackFunction, Message, AutoCorrect) {
        var that = this;
        var cb = function (resp) {
            if (that.MessageShown)
                Page.CloseProcessingMessage();
            var sonuc = JSON.TryParse(resp, null);
            if (sonuc === null)
                return Page.ShowError('Hata: ' + resp);
            if (typeof CallBackFunction == 'function')
                CallBackFunction(sonuc, resp);
        }
        return this.Send(ServerFunc, CustomParams, cb, Message, AutoCorrect);
    };

    /**
     * @returns {Page.Ajax}
     */
    this.CorrectEncoding = function (correct) {
        if (is_set(correct))
            this.AutoCorrect = correct;
        return this;
    };

    /**
     * @returns {Page.Ajax}
     */
    this.SetMessage = function (msg) {
        this.Message = msg;
        return this;
    };

    /**
     * @returns {Page.Ajax}
     */
    this.SetCallback = function (callback) {
        if (typeof callback == "function")
            this.CallBackFunction = callback;
        else if (typeof callback == "number")
            this.RefreshStyle = callback;
        return this;
    };

    this.Url = this.GetUrl();
    this.AutoCorrect = true;
    this.Message = Page.SAVING_MSG;
    this.MessageShown = true;
    this.CallBackFunction = Page.Ajax.GuncelleKapat;
    this.RefreshStyle = Page.Ajax.DefaultRefreshStyle;
};

Page.Ajax.ResponseWaiting = false;

/**
 * @returns {Page.Ajax}
 */
Page.Ajax.Get = function (url) {
    var ajx = new Page.Ajax();
    if (is_set(url) && url != '')
        ajx.Url = GetUrl(url, null, 1);
    return ajx;
};

/**
 * @param {string} ServerFunc Server Funciton Name
 * @param {object} CustomParams
 * @param {function} CallBackFunction
 * @param {string} Message
 * @param {bool} AutoCorrect yazım hatalarını otomatik düzelt, varsayılını true
 * @returns {Page.Ajax}
 */
Page.Ajax.Send = function (ServerFunc, CustomParams, CallBackFunction, Message, AutoCorrect) {
    return Page.Ajax.Get().Send(ServerFunc, CustomParams, CallBackFunction, Message, AutoCorrect);
};

/**
 * IRIT = If Response Is True call CallbackFunction
 * Page.Ajax.Send fonksiyonu ile aynı şekilde çalışır, tek fark CallBackFunction'a sadece işlem
 * sonuc true(1) ise çalıştırır, aksi halde Page.ShowError a gönderir.
 * @param {string} ServerFunc Server Funciton Name
 * @param {object} CustomParams
 * @param {function} CallBackFunction
 * @param {string} Message
 * @param {bool} AutoCorrect
 * @returns {Page.Ajax}
 */
Page.Ajax.SendBool = function (ServerFunc, CustomParams, CallBackFunction, Message, AutoCorrect) {
    return Page.Ajax.Get().SendBool(ServerFunc, CustomParams, CallBackFunction, Message, AutoCorrect);
};

/**
 * IRIJ = If Response Is Json call CallbackFunction
 * Page.Ajax.Send fonksiyonu ile aynı şekilde çalışır, tek fark CallBackFunction'a sadece işlem
 * sonuc Json nesnesi ise çalıştırır, aksi halde Page.ShowError a gönderir.
 * @param {string} ServerFunc Server Funciton Name
 * @param {object} CustomParams
 * @param {function} CallBackFunction
 * @param {string} Message
 * @param {bool} AutoCorrect
 * @returns {Page.Ajax}
 */
Page.Ajax.SendJson = function (ServerFunc, CustomParams, CallBackFunction, Message, AutoCorrect) {
    return Page.Ajax.Get().SendJson(ServerFunc, CustomParams, CallBackFunction, Message, AutoCorrect);
};


/**
 * Verilen parametrelere bağlı olarak json nesnesi olarak veriyi getirir,
 * gelen veri json değilse hata mesajı verir, bu işlem sırasında "yükleniyor" mesajı gösterir
 * @param {string} ServerFunc Server Funciton Name
 * @param {object} CustomParams
 * @param {function} CallBackFunction
 * @returns {Page.Ajax}
 */
Page.Ajax.Load = function (ServerFunc, CustomParams, CallBackFunction) {
    return Page.Ajax.SendJson(ServerFunc, CustomParams, CallBackFunction, Page.LOADING_MSG);
};


Page.Ajax.DO_NOTHING = 0;
Page.Ajax.REFRESH = 1;
Page.Ajax.CLOSE = 2;
Page.Ajax.REFRESH_AND_CLOSE = 3;
Page.Ajax.REFRESH_NO_MSG = 4;
Page.Ajax.REFRESH_WITH_MSG = 5;
Page.Ajax.REFRESH_AND_MSG = 6;

Page.Ajax.DefaultRefreshStyle = Page.Ajax.REFRESH_AND_CLOSE;

Page.Ajax.GuncelleKapat = function (yanit, style) {
    if (!is_set(style))
        style = Page.Ajax.REFRESH_AND_CLOSE;
    if (style == Page.Ajax.REFRESH_AND_MSG)
        return Page.ShowSuccess(yanit);
    if (yanit == '1')
    {
        if (style == Page.Ajax.DO_NOTHING)
            return;
        if (style == Page.Ajax.REFRESH_NO_MSG)
            return Page.Refresh();

        if (window.opener == window || window.opener == null || window.opener.closed)
            Page.ShowSuccess('İşleminiz başarıyla tamamlandı', Page.Refresh);
        else
        {
            switch (style)
            {
                case Page.Ajax.REFRESH_WITH_MSG:
                case Page.Ajax.REFRESH:
                    var cb = function () {
                        Page.Reload(true);
                    };
                    if (style == Page.Ajax.REFRESH)
                        cb();
                    else
                        Page.ShowSuccess('İşleminiz başarıyla tamamlandı', cb);
                    return;
                case Page.Ajax.CLOSE:
                    return window.close();
                case Page.Ajax.REFRESH_AND_CLOSE:
                    return Page.RefreshAndClose();
            }
        }
    } else
        Page.ShowError('Hata: ' + yanit);
};

function is_set(val)
{
    return (typeof val != "undefined");
}

function GenerateCp()
{
    if (typeof cp == "undefined")
    {
        cp = new cpaint();
        cp.set_transfer_mode('post');
        cp.set_response_type('text');
    }
    return cp;
}

String.CleanMsWordChars = function (text, htmlEntities) {
    var swapCodes = new Array(8211, 8212, 8216, 8217, 8220, 8221, 8226, 8230); // dec codes from char at
    var swapStrings = new Array("--", "--", "'", "'", '"', '"', "*", "...");
    if (typeof htmlEntities != "undefined" && htmlEntities == true)
    {
        swapStrings[2] = "&#39;";
        swapStrings[3] = "&#39;";
        swapStrings[4] = "&quot;";
        swapStrings[5] = "&quot;";
    }
    for (var i = 0; i < swapCodes.length; i++)
    {
        var swapper = new RegExp("\\u" + swapCodes[i].toString(16), "g"); // hex codes
        text = text.replace(swapper, swapStrings[i]);
    }
    var entities = [
        /[\u20A0-\u219F]/gim,	// Currency symbols
        /[\u2200-\u22FF]/gim,	// Math operators
        /[\u2000-\u20FF]/gim,  // Weird punctuation
        /[\u0000-\u001F\u0090-\u009F]/gim	// String literal control characters
    ];
    for (var i = 0; i < entities.length; i++)
    {
        var re = new RegExp(entities[i]);
        text = text.replace(re, function (c) {
            if (i == entities.length - 1)
                return '';
            return '&#' + c.charCodeAt(0) + ';';
        });
    }
    return $.trim(text);
};

function ifEmpty(val, defVal)
{
	return val ? val : defVal;
}

Page.OpenNewWindow = function (url, wname, wi, he, extra) {
	var l = 0;
	var t = 0;
	if (typeof wi == "undefined")
		wi = 500;
	if (typeof he == "undefined")
		he = 500;
	if (wi == 'full' || wi == 0)
	{
		wi = window.screen.availWidth;
		l = 0;
	} else
		l = (window.screen.availWidth - wi) / 2;

	if (he == 'full' || he == 0)
	{
		he = window.screen.availHeight;
		if ($.browser.webkit)
			he -= 70;
		t = 0;
	} else
		t = (window.screen.availHeight - he) / 2;

	if (typeof extra == "undefined")
		extra = "menubar=1,scrollbars=1,resizable=1";
	var wnd = window.open(url, wname,
			'width=' + wi + ',' +
			'height=' + he + ',' +
			'left=' + l + ',' +
			'top=' + t + ',' +
			extra
			);
	wnd.blur();
	wnd.focus();
	return wnd;
};

Page.GetParameters = function (url, ignore) {
	if (!is_set(url))
		url = Page.GetWindowHref();
	if (!is_set(ignore))
		ignore = [];
	if (!$.isArray(ignore))
		ignore = [ignore];
	var parts = url.split('?');
	var sonuc = new Object();
	if (parts.length <= 1)
		return sonuc;
	var params = parts[1].split('&');
	for (var i = 0; i < params.length; i++)
	{
		var p = params[i].split('=');
		if (p.length == 2 && p[1] && $.inArray(p[0], ignore) < 0)
			sonuc[p[0]] = unescape(decodeURIComponent(p[1]));
	}
	return sonuc;
};

Page.Params = Page.GetParameters();

function GetUrl(url)
{
	var acts = url.split('.');
	var url = window.location.protocol + "//" + window.location.host + "/?";
	$.each(acts,function(k,v){
		var i = k + 1;
		var s = '&';
		if(k == 0)
		{
			i = "";
			s = "";
		}
		url += s + "act" + i + "=" + v;
	});
	return url;
}

Page.UrlWithActiveTab = function (wnd) {
	wnd = wnd || window;
	var url = wnd.location.href;
	var tab = '';
	try {
		if ($('.tabberlive', wnd.document).length > 0)
			tab = $('.tabberlive', wnd.document).get(0).tabber.getActiveIndex();
		else
			tab = wnd.$('.ui-tabs').tabs("option", "active");
	} catch (e) {
	}
	var sc = $(wnd).scrollTop();
	if (typeof RefreshScrollSelector != 'undefined')
		sc = $(RefreshScrollSelector).offset().top - 100;
	if (typeof wnd.TabIndex != "undefined")
		for (var i = 0; i < wnd.TabIndex; i++)
		{
			tab = wnd.$('.ui-tabs[tab_index=' + i + ']').tabs('option', 'active');
			url = Page.UrlChangeParam('tab' + (i == 0 ? '' : i + 1), tab, url);
		}
	else if (tab > 0)
		url = Page.UrlChangeParam('tab', tab, url);
	if (sc > 0 || Page.GetParameter('scroll') > 0)
		url = Page.UrlChangeParam('scroll', sc, url);

	var bsTabs = [];
	wnd.$('UL.nav-ers-tab LI.active').each(function(){
		bsTabs.push($(this).index());
	});
	if (bsTabs.length > 0)
		url = Page.UrlChangeParam('bs-tabs', bsTabs.join(','), url);
	if (typeof wnd.Ara == 'function')
	{
		var params = wnd.Ara(null, null, true);
		for (var name in params)
			if (Page.GetParameter(name, null, url) === null)
				url += '&' + name + '=' + params[name];
	}

	return url;
};

/**
 * url string ise verilen linke sayfayı yükler, eğer bir object ise mevcut linkte
 * url nesnesindeki değerleri değiştirerek yükler. Örneğin; url = {id: 2} ise
 * adres satırındaki id değerini 2 yarak tekrar yükler
 * @param {string|object} url
 */
Page.Load = function (url, baseUrl) {
	if (typeof url == 'object')
	{
		var newUrl = baseUrl || Page.UrlWithActiveTab();
		for(var i in url)
			newUrl = Page.UrlChangeParam(i, url[i], newUrl);
		url = newUrl;
	}
	window.location.href = url;
};

Page.Refresh = function (win) {
	if (typeof win != 'object' || typeof win.document != 'object')
		win = window;

	if (win.lastDataTableObj && ! win.lastDataTableObj.ShowPaging)
		return win.Page.Load(Page.UrlWithActiveTab(win));

	if (typeof win['RefreshPageFunc'] == 'function')
		return win['RefreshPageFunc']();

	if (win.Page)
		return win.Page.Load(Page.UrlWithActiveTab(win));
	win.location.reload();
};
Page.RefreshAndClose = function () {
	Page.Refresh(opener);
	if (opener && Page.GetParameter("mode") != '')
		close();
};

/** EXTRA FONKSİYONLAR**/
function IletisimeGec()
{
	var obj = {};
	obj.ad = $('#ziyaretci_ad').val();
	obj.email = $('#ziyaretci_email').val();
	obj.mesaj = $('#ziyaretci_mesaj').val();



	if(obj.ad == "" || obj.email == "" || obj.mesaj == "")
		return Page.ShowError("Lütfen belirtilen alanları alanları doldurunuz..");

	Page.Ajax.Get('ajax').SendBool("IletisimeGec",obj, function(){
		$('#ziyaretci_ad,#ziyaretci_email,#ziyaretci_mesaj').val("");
		return Page.ShowSuccess("Mesajınız tarafımıza iletilmiştir. En kısa sürede geri dönüş sağlayacağız.");
	});
}
